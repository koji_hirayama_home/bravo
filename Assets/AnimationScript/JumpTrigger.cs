﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Main;

public class JumpTrigger : StateMachineBehaviour {
	public static bool isJumpTrigger;
	bool isJump;
	GameObject[] obstacles;
	GameObject player;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        isJumpTrigger = true;
        isJump = true;
        player = GameObject.FindGameObjectWithTag ("Player");
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (player.transform.position.y > 5f && isJump == true) {
			isJump = false;
			obstacles = GameObject.FindGameObjectsWithTag ("Obstacle");
			foreach (GameObject obstacle in obstacles) {
				obstacle.GetComponent<BoxCollider> ().enabled = false;
			}
		} else if(player.transform.position.y <= 5f && isJump == false){
			obstacles = GameObject.FindGameObjectsWithTag ("Obstacle");
			foreach (GameObject obstacle in obstacles) {
				obstacle.GetComponent<BoxCollider> ().enabled = true;
				isJump = true;
			}
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		isJumpTrigger = false;
		obstacles = GameObject.FindGameObjectsWithTag ("Obstacle");
		foreach (GameObject obstacle in obstacles) {
			obstacle.GetComponent<BoxCollider> ().enabled = true;
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
