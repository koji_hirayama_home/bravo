﻿Shader "Custom/MoveFloor" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0
		sampler2D _MainTex;
		struct Input {
			float2 uv_MainTex;
		};
		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed2 uv = IN.uv_MainTex;
			uv.x += _Time * -20;
			 fixed4 c = tex2D (_MainTex,uv);
			 o.Albedo = c.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
