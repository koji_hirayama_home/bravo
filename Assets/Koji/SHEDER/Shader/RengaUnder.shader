﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/RengaUnder" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
//		_DisolveTex ("DisolveTex (RGB)", 2D) = "white" {}
		_SubTex ("Sub Texture", 2D) = "white" {}
		_MaskTex ("Mask Texture", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard 
		#pragma target 3.0

		sampler2D _MainTex;
//		sampler2D _DisolveTex;
		sampler2D _SubTex;
		sampler2D _MaskTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		UNITY_INSTANCING_BUFFER_START(Props)
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
//
//			//ディゾルブの値
//			//tex2D関数は、UV座標(uv_MainTex)からテクスチャ(_MainTex)上のピクセルの色を計算して返します。
//			fixed4 m = tex2D(_DisolveTex,IN.uv_MainTex);
//			//ディゾルブの明度を出す
//			half g  = m.r *0.2 + m.g*0.7+m.b*0.1;
//
//			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
//			o.Albedo = c.rgb;
//			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
//			o.Smoothness = _Glossiness;
//			o.Alpha = 0.9;
			fixed4 c1 = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 c2 = tex2D (_SubTex,  IN.uv_MainTex);
			fixed4 p  = tex2D (_MaskTex, IN.uv_MainTex);
			o.Albedo = lerp(c1, c2, p);			

		}
		ENDCG
	}
	FallBack "Diffuse"
}
