﻿//テクスチャを上下左右にスライドさせる

Shader "Unlit/CurevedUnlitMove" {

Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MoveX ("MoveX", float) = 0
        _MoveY ("MoveY", float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 color : TEXCOORD2;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _CurveStrength;
            float _CurveStrengthX;
            float _MoveX;
            float _MoveY;

            v2f vert(appdata v)
            {
                v2f o;

                float _Horizon = 100.0f;
                float _FadeDist = 50.0f;

                o.vertex = UnityObjectToClipPos(v.vertex);

                float dist = UNITY_Z_0_FAR_FROM_CLIPSPACE(o.vertex.z);
                #if UNITY_UV_STARTS_AT_TOP
                    o.vertex.y += _CurveStrength * dist * dist;
                    o.vertex.x += _CurveStrengthX * dist * dist;
                #else
                    o.vertex.y -= _CurveStrength * dist * dist;
                    o.vertex.x -= _CurveStrengthX * dist * dist;
                #endif

                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv.x += _Time*_MoveX;
                o.uv.y += _Time*_MoveY;
                fixed4 c = tex2D (_MainTex,o.uv);
                o.color = v.color;

                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * i.color;
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}