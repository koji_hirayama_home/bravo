﻿//--------------------------------------------------------------------------------------------------
// 2枚のテクスチャをマスクでブレンドする
// 反転機能あり
//--------------------------------------------------------------------------------------------------
Shader "Unlit/TextureBlend" {

	Properties
	{
		[MaterialToggle] _Mirror ("isMirror", Float) = 0 // 0 is false, 1 is true
		[Space]
		_MainTex ("Texture", 2D) = "white" {}
		_SubTex ("Sub Texture", 2D) = "white" {}
		_MaskTex ("Mask Texture", 2D) = "white" {}	
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue" = "Transparent"}
		LOD 100
			ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask RGB
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 color : TEXCOORD2;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _SubTex;
			sampler2D _MaskTex;
			float4 _MainTex_ST;
			float _CurveStrength;
			float _CurveStrengthX;
			bool _Mirror;

			v2f vert(appdata v)
			{
				v2f o;

				float _Horizon = 100.0f;
				float _FadeDist = 50.0f;

				o.vertex = UnityObjectToClipPos(v.vertex);
				float dist = UNITY_Z_0_FAR_FROM_CLIPSPACE(o.vertex.z);
				#if UNITY_UV_STARTS_AT_TOP
					o.vertex.y += _CurveStrength * dist * dist;
					o.vertex.x += _CurveStrengthX * dist * dist;
				#else
					o.vertex.y -= _CurveStrength * dist * dist;
					o.vertex.x -= _CurveStrengthX * dist * dist;
				#endif
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color ;
				UNITY_TRANSFER_FOG(o, o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 c1 = tex2D (_MainTex, i.uv);
				fixed4 c2 = tex2D (_SubTex,  i.uv);
				fixed4 p  = tex2D (_MaskTex, i.uv);

				if(_Mirror){
					p=1-p;
				}
				fixed4 c = lerp(c1, c2, p);	
				//グレースケール値取得
				half g = p.r * 0.2 + p.g * 0.7 + p.b * 0.1;
				if(g < 0.5f){
					//一枚目の画像なら、１枚目の画像のアルファ値を適用
					c.a=c1.a;
				}else{
					//２枚目の画像なら、１枚目の画像のアルファ値を適用
					c.a=c2.a;
				}
				UNITY_APPLY_FOG(i.fogCoord, c);
				return c;
			}
			ENDCG
		}
	}
}
