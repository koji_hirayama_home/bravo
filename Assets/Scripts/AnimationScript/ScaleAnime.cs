﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAnime : MonoBehaviour {
    [SerializeField] float speed;
    [SerializeField] float r;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float scaleAnim = Mathf.Abs(Mathf.Sin(Time.time * speed) * r) + 1f;
        this.transform.localScale = new Vector3(scaleAnim, scaleAnim, scaleAnim);
	}
}
