﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


namespace Main{

    public class AudioManager : MonoBehaviour
    {
        [SerializeField] AudioClip coinGetCip;
        [SerializeField] AudioClip bravo_Move;
        [SerializeField] AudioClip titleBgmCip;
        [SerializeField] AudioClip playBgmCip;
        [SerializeField] AudioClip bravo_DamagCip;
        [SerializeField] AudioClip buttonCip;
        [SerializeField] AudioClip countUpCip;
        [SerializeField] AudioClip flightItemCip;




        public static AudioClip coinGetSE;
        public static AudioClip bravo_MoveSE;
        public static AudioClip bravo_DamageSE;
		public static AudioClip titleBGM;
        public static AudioClip playBGM;
        public static AudioClip buttonSE;
        public static AudioClip countUpSE;
        public static AudioClip flightItemSE;


        public static AudioSource audio;
        public static AudioSource playerAudio;

        // Use this for initialization
        void Start()
        {
            audio = this.transform.GetComponent<AudioSource>();
            playerAudio = GameObject.Find("Player").GetComponent<AudioSource>();

            coinGetSE = coinGetCip;
            bravo_MoveSE = bravo_Move;
            bravo_DamageSE = bravo_DamagCip;
            titleBGM = titleBgmCip;
            playBGM = playBgmCip;
            buttonSE = buttonCip;
            countUpSE = countUpCip;
            flightItemSE = flightItemCip;

            audio.volume = 1;

        }

        // Update is called once per frame
        //void Update()
        //{
        //    if(audio.volume == 0){
        //        playerAudio.volume = 0;
        //    }
        //}


        public static void CoinGetSound()
        {
            audio.volume = 1;
            audio.PlayOneShot(coinGetSE);

        }

        public static void Bravo_MoveSound()
        {
            playerAudio.volume = 0.15f;
            playerAudio.PlayOneShot(bravo_MoveSE);
        }

        public static void Bravo_DamageSound(){
            audio.PlayOneShot(bravo_DamageSE);
        }

        public static void Button_Sound(){
            audio.PlayOneShot(buttonSE);
        }

        public static void CountUp_Sound(){
            audio.PlayOneShot(countUpSE);
        }


        public static void FlightItem_Sound(){
            audio.PlayOneShot(flightItemSE);
        }

        public static IEnumerator PlayBgmCorutine()
        {
            //FadeOut_BGM
            yield return new WaitForSeconds(FadeOutSound(3f,0.1f));
            yield return new WaitForSeconds(FadeInSound(playBGM, 3f));

        }

        public static IEnumerator GameOverBgmFadeOutCotutine(){
            yield return new WaitForSeconds(FadeOutSound(1f,0.4f));
        }


        public static float FadeOutSound (float time,float vule){
            DOTween.To(
                () => (float)audio.volume, 
                vol => audio.volume = (float)vol, 
                vule, 
                time);
            return time;
        }


        public static float FadeInSound(AudioClip clip,float time)
        {
            audio.clip = clip;
            audio.Play();
            DOTween.To(
                () => (float)audio.volume,
                vol => audio.volume = (float)vol,
                1f,
                time);
            return time;
        }
	}
	
}