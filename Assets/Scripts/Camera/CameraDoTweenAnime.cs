﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Main{

public class CameraDoTweenAnime : MonoBehaviour {
		[SerializeField]GameObject startCameraPos;
		[SerializeField]GameObject playCameraPos;
	// Use this for initialization
	void Start () {
			this.transform.eulerAngles = new Vector3 (0, 160f, 0);
			Camera.main.transform.localPosition = startCameraPos.transform.localPosition;
            Camera.main.transform.localRotation = startCameraPos.transform.localRotation;
	}
	
	
		public void StartCameraDoTweenAnime(){
            this.transform.DOLocalRotate(Vector3.zero, 7f);//.SetEase(Ease.InOutBack);
			Camera.main.transform.DOLocalMove (playCameraPos.transform.localPosition, 5f).SetEase(Ease.OutBack);
			Camera.main.transform.DOLocalRotate (playCameraPos.transform.transform.localEulerAngles, 5);
		}
	}
}
