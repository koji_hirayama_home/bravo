﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Main
{

    public class ShakeCamera : MonoBehaviour
    {
        public static float shake;
        Vector3 defaultPosition;
        //[SerializeField] Transform startPos;
        //[SerializeField] Transform playPos;
        [SerializeField] GameObject player;

        void start()
        {
            shake = 0;
            defaultPosition = this.transform.localPosition;
        }


        void Update()
        {
            shake = Mathf.Clamp(shake, 0, 1f);
            if (player.GetComponent<PlayerController>().isPlayerController == false && player.GetComponent<PlayerCollision>().enabled == false)
            {
                if (shake > 0)
                {
                    
                    //defaultPosition = playPos.localPosition;
                    //カメラのポジションにランダムな値を入れて揺らす
                    Vector3 randomPosition;
                    randomPosition.x = Random.Range(shake * -1, shake);
                    randomPosition.y = Random.Range(shake * -1, shake);
                    randomPosition.z = Random.Range(shake * -1, shake);

                    transform.localPosition = defaultPosition + randomPosition;

                    shake -= Time.deltaTime;
                }
                else
                {
                    transform.localPosition = defaultPosition;
                }
            }
        }
    }
}

