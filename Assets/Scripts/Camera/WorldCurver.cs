﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WorldCurver : MonoBehaviour {
    [Range(-0.001f,0.001f)]
    public float curveStrength;
    [Range(-0.001f, 0.001f)]
    public float curveStrengthX;

	
	// Update is called once per frame
	void Update () {
        Shader.SetGlobalFloat("_CurveStrength", curveStrength);
        Shader.SetGlobalFloat("_CurveStrengthX", curveStrengthX);
	}
}
