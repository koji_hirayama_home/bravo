﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

    public class DesginContainer : MonoBehaviour
    {
        public List<GameObject> desginPartList = new List<GameObject>();
        float acriveTime;


        public void AddContainer(GameObject part)
        {
            //desginPartList.Add(part);
            part.SetActive(false);
            StartCoroutine(ContainerActive(part, 0.3f));
        }


        IEnumerator ContainerActive(GameObject part ,float time){
            acriveTime += time;
            yield return new WaitForSeconds(acriveTime);
            part.SetActive(true);
        }
    }
}
