﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Main
{

    public class ItemCollision : HitManager
    {
        public ItemType itemType;
        bool isStartPositionComplete = false;


        // Use this for initialization
        void Start()
        {
            //遅延させてアイテムをを障害物の上に載せるか載せなきかの判定する
            Invoke("IsSetStartPositinOnComplete", 1f);
        }


        void OnTriggerEnter(Collider col)
        {
            //プレイヤーと接触時
            if (col.gameObject.tag == "Player")
            {
                Destroy(this.gameObject);
                hitDispatcher.itemHit.OnNext(itemType);
            }



            //障害物との接触時（１秒間くらいで判定）
            if (col.gameObject.tag == "Obstacle" && isStartPositionComplete == false)
            {
                //アイテムの高さ調整（障害物の上に乗っているアイテムもあるため）
                float height = 0;
                //コリジョンした障害物のitemPosが存在したら実行
                if (col.gameObject.GetComponent<ObstacleCollision>().itemPos != null)
                {
                    //障害物の上にアイテムを置くためのポジション
                    height = col.gameObject.GetComponent<ObstacleCollision>().itemPos.position.y;
                    switch (itemType)
                    {
                        //ジャンプゴールド用だけどうまく起動しない（あとで直す）
                        case ItemType.JumpGold:
                            GameObject jumpGold = this.gameObject.transform.parent.gameObject;
                            jumpGold.transform.position = new Vector3(transform.position.x, height, jumpGold.transform.position.z);
                            jumpGold.gameObject.transform.SetParent(col.gameObject.transform);
                            break;
                        default:
                            this.gameObject.transform.position = new Vector3(transform.position.x, height, transform.position.z);
                            this.gameObject.transform.SetParent(col.gameObject.transform);
                            break;
                    }
                }
            }
        }

        //遅延させてアイテムをを障害物の上に載せるか載せなきかの判定する（Invokeを使って遅延処理を約１秒で判断させる）
        void IsSetStartPositinOnComplete()
        {
            isStartPositionComplete = true;
        }
    }
}
