﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{
	//アイテム一覧enum
	public enum ItemType{
		Gold,
		JumpGold,
		FlightItem,
		HpRecoveryItem,
	}

	public class ItemInfo : DesginCommon {
		//コンストラクタで親にアイテムタイプの情報を渡す
		public ItemInfo (ItemType itemtype){
			this.itemType = itemtype;
		}


		//アイテム参照パス
		public static Dictionary<ItemType,string> itemPath = new Dictionary<ItemType, string> (){
			{ItemType.Gold,"Prefabs/Items/Gold"},
			{ItemType.JumpGold,"Prefabs/Items/JumpGold"},//なんかジャンプゴールドうまく起動しない（後回し）
			{ItemType.FlightItem,"Prefabs/Items/FlightItem"},
			{ItemType.HpRecoveryItem,"Prefabs/Items/HpRecoveryItem"},
		};

	
	}
}
