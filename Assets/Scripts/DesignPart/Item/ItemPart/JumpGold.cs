﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

public class JumpGold : MonoBehaviour {
		[SerializeField]GameObject gold;
		[SerializeField]int goldInstantiateCount = 6;
		[SerializeField]float r = 3;
		[SerializeField]float angle = 180;
	// Use this for initialization
	void Start () {
			ArchGoldPos ();
	}
	
	
		void ArchGoldPos(){
			float x = 0;
			float y = 0;
			float z = 0;
			float degree = angle/goldInstantiateCount * Mathf.Deg2Rad;
			for (int i = 0; i < goldInstantiateCount - 1; i++) {
				GameObject obj = Instantiate (gold,Vector3.zero,Quaternion.identity,this.transform);
				y = Mathf.Sin (degree * i) * r/2;
				z = Mathf.Cos (degree * i) * r;
				obj.transform.localPosition = new Vector3 (x, y, -z);
			}
//			y = Mathf.Sin (degree * goldInstantiateCount) * r/2f;
//			z = Mathf.Cos (degree * goldInstantiateCount) * r;
//			gold.transform.localPosition = new Vector3 (x, y, -z);
			Destroy(gold);
		}
	}
}
