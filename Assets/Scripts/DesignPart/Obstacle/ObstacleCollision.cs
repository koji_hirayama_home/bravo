﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Main
{

	public class ObstacleCollision : HitManager
	{
		public ObstacleType obstacleType;

		//障害物の上にアイテムを載せる時の高さ習得用
		public Transform itemPos;

       

		// Use this for initialization
		void Start ()
		{
			gameObject.layer = LayerMask.NameToLayer ("Obstacle");
		}
	
		// Update is called once per frame
		void Update ()
		{
			//プレイヤーがダウン動作時（障害物の下をくぐる）のコリジョン判定の切り替え
			PlayerDownCollision (DownTrigger.isDown);

		}


		//プレイヤーがダウン動作時（障害物の下をくぐる）のコリジョン判定の切り替え
		public void PlayerDownCollision (bool isCollision)
		{
			//特定の障害物のみ対応
			switch (obstacleType) {
			case ObstacleType.Board:
			case ObstacleType.HurdleBig:
			case ObstacleType.HurdleLow:
				this.GetComponent<BoxCollider> ().enabled = !isCollision;
				break;
			}
		}


		//この辺模索中
		//当たり判定プレイヤー側でやるかも
		void OnCollisionEnter (Collision col)
		{
			if (col.gameObject.tag == "Player") {
				//hitDispatcher.obstacleHit.OnNext (this.obstacleType);//テスト適当なOnNext後で修正


			}
		}


	}
}
