﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main {

	public enum ObstacleType{
		Car,
		Flar,
		Board,
		BigFlar,
		BigSakaFlar,
		Box,
		BoxMedium,
		BoxLarge,
		TRAIN,
		TRAIN2,
		TRAIN3,
		TRAIN_Lv1,
		TRAIN_Lv2,
		TRAIN_Lv3,
		TRAIN2_Lv1,
		TRAIN2_Lv2,
		TRAIN2_Lv3,
		TRAIN3_Lv1,
		TRAIN3_Lv2,
		TRAIN3_Lv3,
		HurdleBig,
		HurdleLow,
		Hurdle,
        CAR_Lv1,
        CAR_Lv2,
        CAR_Lv3,
	}

	public class ObstacleInfo : DesginCommon {
		
		//コンストラクタで親にアイテムタイプの情報を渡す
		public ObstacleInfo(ObstacleType obstacleType){
			this.obstacleType = obstacleType;
		}


		//アイテムを生成するときにどのアイテムタイプを生成するのかのパス
		public static Dictionary<ObstacleType,string> obstaclePath = new Dictionary<ObstacleType, string> () {
			{ ObstacleType.Car,"Prefabs/Obstacles/Car" },
			{ ObstacleType.Flar,"Prefabs/Obstacles/Flar"},
			{ ObstacleType.BigFlar,"Prefabs/Obstacles/BigFlar"},
			{ ObstacleType.BigSakaFlar,"Prefabs/Obstacles/BigSakaFlar"},
			{ ObstacleType.Board,"Prefabs/Obstacles/Board"},
			{ ObstacleType.Box,"Prefabs/Obstacles/Box"},
			{ ObstacleType.BoxMedium,"Prefabs/Obstacles/BoxMedium"},
			{ ObstacleType.BoxLarge,"Prefabs/Obstacles/BoxLarge"},
			{ ObstacleType.TRAIN,"Prefabs/Obstacles/Train"},
			{ ObstacleType.TRAIN_Lv1,"Prefabs/Obstacles/Train_Lv1"},
			{ ObstacleType.TRAIN_Lv2,"Prefabs/Obstacles/Train_Lv2"},
			{ ObstacleType.TRAIN_Lv3,"Prefabs/Obstacles/Train_Lv3"},
			{ ObstacleType.TRAIN2,"Prefabs/Obstacles/Train2"},
			{ ObstacleType.TRAIN2_Lv1,"Prefabs/Obstacles/Train2_Lv1"},
			{ ObstacleType.TRAIN2_Lv2,"Prefabs/Obstacles/Train2_Lv2"},
			{ ObstacleType.TRAIN2_Lv3,"Prefabs/Obstacles/Train2_Lv3"},
			{ ObstacleType.TRAIN3,"Prefabs/Obstacles/Train3"},
			{ ObstacleType.TRAIN3_Lv1,"Prefabs/Obstacles/Train3_Lv1"},
			{ ObstacleType.TRAIN3_Lv2,"Prefabs/Obstacles/Train3_Lv2"},
			{ ObstacleType.TRAIN3_Lv3,"Prefabs/Obstacles/Train3_Lv3"},
			{ ObstacleType.HurdleBig,"Prefabs/Obstacles/BigHurdle"},
			{ ObstacleType.HurdleLow,"Prefabs/Obstacles/LowHurdle"},
			{ ObstacleType.Hurdle,"Prefabs/Obstacles/Hurdle"},
            { ObstacleType.CAR_Lv1,"Prefabs/Obstacles/Car_Lv1" },
            { ObstacleType.CAR_Lv2,"Prefabs/Obstacles/Car_Lv2" },
            { ObstacleType.CAR_Lv3,"Prefabs/Obstacles/Car_Lv3" },
		};

	
	}
}
