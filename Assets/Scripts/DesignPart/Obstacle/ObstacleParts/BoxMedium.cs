﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

public class BoxMedium : MonoBehaviour
{
    [SerializeField] GameObject childObj;

    // Use this for initialization
    void Start()
    {
        ChildPosition(childObj);
    }



    //配置位置が微妙になるため子オブジェクトの原点の位置を変えて配置する
    void ChildPosition(GameObject child)
    {
        //真ん中以外
        if (transform.position.x != 0)
        {
            child.transform.localPosition = new Vector3(transform.position.x / -2, 0, 0);
        }
        else
        {
            //真ん中に配置するときは、ランダムにどっちらかのサイドに移動
            int dice = Random.Range(0, 2);
            float x = 0;
            switch (dice)
            {
                case 0:
                    x = 5f;
                    break;
                case 1:
                    x = -5f;
                    break;
            }
            child.transform.localPosition = new Vector3(x, 0, 0);
        }
    }
}
}
