﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSelect : MonoBehaviour {
    [SerializeField] GameObject[] cars;

	// Use this for initialization
	void Start () {
        int dice = Random.Range(0, cars.Length);
        cars[dice].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
