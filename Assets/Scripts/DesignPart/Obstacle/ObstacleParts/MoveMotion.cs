﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class MoveMotion : MonoBehaviour
	{
		public enum MoveSpeed
		{
			NULL,
			LEVEL1,
			LEVEL2,
			LEVEL3,
		}

		public MoveSpeed moveSpeed = MoveSpeed.NULL;

		GameObject player;

		//動く障害物が（TRAINとか）動き始めるPlayerとの距離
		float targetDistance = 300f;
		//Playerと動く障害物との距離を計算
		float distance;
		//接近スピード
		float speed = 50f;

		// Use this for initialization
		void Start ()
		{
			player = GameObject.Find ("Player");

			//Levelに応じてスピード変更
			switch (moveSpeed) {
			case MoveSpeed.LEVEL1:
				speed = 50f;
				break;
			case MoveSpeed.LEVEL2:
				speed = 75f;
				break;
			case MoveSpeed.LEVEL3:
				speed = 100f;
				break;
			}
		}
	
		// Update is called once per frame
		void Update ()
		{
			//プレイヤーが起動している時。
			if (player.GetComponent<PlayerController> ().isPlayerController == true) {
                transform.GetComponent<Rigidbody>().isKinematic = false;
				//enumの値がnullじゃない時に動く
			if (moveSpeed != MoveSpeed.NULL) {
					//距離計算
				distance = Vector3.Distance (transform.position, player.transform.position);
					//計算した距離に対して指定した距離の範囲にPlayerが入ったら動く
					if (distance < targetDistance) {
						//前進させる
						transform.Translate (Vector3.back * speed * Time.deltaTime);
					}
				}
            }else{
                transform.GetComponent<Rigidbody>().isKinematic = true;
            }
		}
	}
}
