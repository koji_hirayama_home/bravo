﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Main{

	public class HitDispatcher {
	
	private static HitDispatcher mInstance;

	private HitDispatcher () { // Private Constructor

			//Debug.Log("Create HitDispatcherSingleton instance.");
	}

	public static HitDispatcher Instance {

		get {

			if( mInstance == null ) mInstance = new HitDispatcher();

			return mInstance;
		}
	}


	//障害物にぶつかった時のUniRx
		public Subject<ObstacleType> obstacleHit = new Subject<ObstacleType>();
	    public IObservable<ObstacleType> ObstacleHit {
			get{ return obstacleHit; }
		}


		//アイテム入手時のUniRx
		public Subject<ItemType> itemHit = new Subject<ItemType>();
		public IObservable<ItemType> ItemHit {
			get{ return itemHit; }
		}
	}
}