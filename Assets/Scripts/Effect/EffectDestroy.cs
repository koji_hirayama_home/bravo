﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class EffectDestroy : MonoBehaviour
	{
		
		// Use this for initialization
		void Start ()
		{
			Destroy (gameObject, this.gameObject.GetComponent<ParticleSystem> ().duration);
		}
		
		
	}
}
