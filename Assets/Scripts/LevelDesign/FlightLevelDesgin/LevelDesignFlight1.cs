﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class LevelDesignFlight1 : LevelDesignCommon
	{
	
		//コンストラクタを使ってレベルデザインをどこかで（まだ考え中）グループ化
		//配置間隔は200÷パターンの個数。現在パターンの数10個＝20m間隔(フライトの場合)
		public LevelDesignFlight1 ()
		{

			//アイテムのデザインパターンList
			itemPattern = new ArrayList () {
				new ArrayList (){ null, GOLD, null },
				new ArrayList (){ null, GOLD, null },
				new ArrayList (){ null, GOLD, null },
				new ArrayList (){ null, null, GOLD },
				new ArrayList (){ null, null, GOLD },
				new ArrayList (){ null, null, GOLD },
			};


			//障害物のデザインパターンList
			//長いやつは７個目までに置く！（先端がはみ出ちゃうから）
//			obstaclePattern = new ArrayList () {
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//				new ArrayList (){ null, null, null },
//			};
		}
	}
}
