﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

	public class LevelDesignA6 : LevelDesignCommon {
		
		//コンストラクタを使ってレベルデザインをどこかで（まだ考え中）グループ化
		//配置間隔は360÷パターンの個数。現在パターンの数１２個＝30m間隔
		//(終点は330mになる。次のレーンの始点とかさならないようにするため)
		public LevelDesignA6(){

			//アイテムのデザインパターンList
			itemPattern = new ArrayList () {
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, GOLD, null },
                new ArrayList (){ null, GOLD, null },
                new ArrayList (){ GOLD, null, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, FLIGHT, GOLD },
                new ArrayList (){ null, null, GOLD },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
			};


			//障害物のデザインパターンList
			//長いやつは７個目までに置く！（先端がはみ出ちゃうから）
			obstaclePattern = new ArrayList () {
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, BIGSAKAFLAR },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, BIGFLAR, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ FLAR, null, null },
                new ArrayList (){ CAR_Lv1, null, BIGSAKAFLAR },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, HURDLE_Big, null },
				new ArrayList (){ null, null, null },
			};
		}
	}
}
