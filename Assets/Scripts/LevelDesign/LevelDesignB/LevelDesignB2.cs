﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

	public class LevelDesignB2 : LevelDesignCommon {
		
		//コンストラクタを使ってレベルデザインをどこかで（まだ考え中）グループ化
		//配置間隔は360÷パターンの個数。現在パターンの数１２個＝30m間隔
		//(終点は330mになる。次のレーンの始点とかさならないようにするため)
		public LevelDesignB2(){

			//アイテムのデザインパターンList
			itemPattern = new ArrayList () {
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ JUMPGOLD, null, null },
				new ArrayList (){ null, null, null },
			};


			//障害物のデザインパターンList
			//長いやつは７個目までに置く！（先端がはみ出ちゃうから）
			obstaclePattern = new ArrayList () {
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null},
                new ArrayList (){ null, null, CAR_Lv1 },
				new ArrayList (){ CAR_Lv2, CAR_Lv2, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ BOXLARGE, CAR_Lv2, CAR_Lv2 },
                new ArrayList (){ CAR_Lv1, null, CAR_Lv1 },
			};
		}
	}
}
