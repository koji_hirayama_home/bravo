﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

	public class LevelDesignB5 : LevelDesignCommon {
		
		//コンストラクタを使ってレベルデザインをどこかで（まだ考え中）グループ化
		//配置間隔は360÷パターンの個数。現在パターンの数１２個＝30m間隔
		//(終点は330mになる。次のレーンの始点とかさならないようにするため)
		public LevelDesignB5(){

			//アイテムのデザインパターンList
			itemPattern = new ArrayList () {
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, GOLD, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, JUMPGOLD, null },
				new ArrayList (){ null, null, null },
			};


			//障害物のデザインパターンList
			//長いやつは７個目までに置く！（先端がはみ出ちゃうから）
			obstaclePattern = new ArrayList () {
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, FLAR, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, HURDLE_Big, null},
				new ArrayList (){ null, null, null },
                new ArrayList (){ CAR_Lv3, null, CAR_Lv2 },
                new ArrayList (){ HURDLE_Big, HURDLE_Low, HURDLE_Big },
                new ArrayList (){ CAR_Lv2, null, CAR_Lv1},
			};
		}
	}
}
