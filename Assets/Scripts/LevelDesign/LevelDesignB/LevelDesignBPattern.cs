﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

public class LevelDesignBPattern : MonoBehaviour {
	public static ArrayList patternB = new ArrayList () {
            new LevelDesignStart1 (),
            new LevelDesignStart2 (),
            new LevelDesignStart3 (),
            new LevelDesignStart4 (),
            new LevelDesignStart5 (),
            new LevelDesignStart6 (),
            new LevelDesignStart7 (),
            new LevelDesignStart8 (),
            new LevelDesignStart9 (),
            new LevelDesignStart10 (),
            new LevelDesignStart11 (),
            new LevelDesignStart12 (),
            new LevelDesignStart13 (),
            new LevelDesignStart14 (),
            new LevelDesignStart15 (),
            new LevelDesignA1(),
            new LevelDesignA2(),
            new LevelDesignA3(),
            new LevelDesignA4(),
            new LevelDesignA5(),
            new LevelDesignA6(),
            new LevelDesignA7(),
            new LevelDesignA8(),
            new LevelDesignA9(),
            new LevelDesignA10(),
            new LevelDesignA11(),
            new LevelDesignA12(),
            new LevelDesignA13(),
            new LevelDesignA14(),
            new LevelDesignA15(),
            new LevelDesignB1(),
            new LevelDesignB2(),
            new LevelDesignB3(),
            new LevelDesignB4(),
            new LevelDesignB5(),
		};

	
	}
}
