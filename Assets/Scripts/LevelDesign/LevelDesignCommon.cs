﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

public class LevelDesignCommon  {
		//ここのListを参照して作ったレベルデザインを参照する
		public ArrayList itemPattern;
		public ArrayList obstaclePattern;


	//ステージのレベルデザインを配置するときの省略化記号
		//アイテムを参照する省略化記号
		public ItemInfo GOLD = new ItemInfo(ItemType.Gold);
		public ItemInfo JUMPGOLD = new ItemInfo (ItemType.JumpGold);
		public ItemInfo FLIGHT = new ItemInfo (ItemType.FlightItem);
		public ItemInfo HPRECOVERY = new ItemInfo (ItemType.HpRecoveryItem);





		//障害物を参照する省略化記号
		public ObstacleInfo CAR = new ObstacleInfo(ObstacleType.Car);
        public ObstacleInfo FLAR = new ObstacleInfo(ObstacleType.Flar);//長さ100m
		public ObstacleInfo BIGFLAR = new ObstacleInfo(ObstacleType.BigFlar);//長さ100m
		public ObstacleInfo BIGSAKAFLAR = new ObstacleInfo(ObstacleType.BigSakaFlar);//長さ110m
		public ObstacleInfo BOARD = new ObstacleInfo (ObstacleType.Board);
		public ObstacleInfo BOX = new ObstacleInfo (ObstacleType.Box);
		public ObstacleInfo BOXMEDIUM = new ObstacleInfo (ObstacleType.BoxMedium);
		public ObstacleInfo BOXLARGE = new ObstacleInfo (ObstacleType.BoxLarge);
		public ObstacleInfo TRAIN = new ObstacleInfo (ObstacleType.TRAIN);//長さ80m
		public ObstacleInfo TRAIN_Lv1 = new ObstacleInfo (ObstacleType.TRAIN_Lv1);//長さ80m
		public ObstacleInfo TRAIN_Lv2 = new ObstacleInfo (ObstacleType.TRAIN_Lv2);//長さ80m
		public ObstacleInfo TRAIN_Lv3 = new ObstacleInfo (ObstacleType.TRAIN_Lv3);//長さ80m
		public ObstacleInfo TRAIN2 = new ObstacleInfo (ObstacleType.TRAIN2);//長さ65m
		public ObstacleInfo TRAIN2_Lv1 = new ObstacleInfo (ObstacleType.TRAIN2_Lv1);//長さ65m
		public ObstacleInfo TRAIN2_Lv2 = new ObstacleInfo (ObstacleType.TRAIN2_Lv2);//長さ65m
		public ObstacleInfo TRAIN2_Lv3 = new ObstacleInfo (ObstacleType.TRAIN2_Lv3);//長さ65m
		public ObstacleInfo TRAIN3 = new ObstacleInfo (ObstacleType.TRAIN3);//長さ180m
		public ObstacleInfo TRAIN3_Lv1 = new ObstacleInfo (ObstacleType.TRAIN3_Lv1);//長さ180m
		public ObstacleInfo TRAIN3_Lv2 = new ObstacleInfo (ObstacleType.TRAIN3_Lv2);//長さ180m
		public ObstacleInfo TRAIN3_Lv3 = new ObstacleInfo (ObstacleType.TRAIN3_Lv3);//長さ180m
		public ObstacleInfo HURDLE_Big = new ObstacleInfo (ObstacleType.HurdleBig);
		public ObstacleInfo HURDLE_Low = new ObstacleInfo (ObstacleType.HurdleLow);
		public ObstacleInfo HURDLE = new ObstacleInfo (ObstacleType.Hurdle);
        public ObstacleInfo CAR_Lv1 = new ObstacleInfo(ObstacleType.CAR_Lv1);
        public ObstacleInfo CAR_Lv2 = new ObstacleInfo(ObstacleType.CAR_Lv2);
        public ObstacleInfo CAR_Lv3 = new ObstacleInfo(ObstacleType.CAR_Lv3);

	}
}
