﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class LevelDesignStart3 : LevelDesignCommon
	{
	
		//コンストラクタを使ってレベルデザインをどこかで（まだ考え中）グループ化
		//配置間隔は330÷パターンの個数。現在パターンの数１1個＝30m間隔
		public LevelDesignStart3 ()
		{

			//アイテムのデザインパターンList
			itemPattern = new ArrayList () {
				new ArrayList (){ null, GOLD, null },
				new ArrayList (){ null, GOLD, null },
				new ArrayList (){ GOLD, null, null },
				new ArrayList (){ null, GOLD, null },
				new ArrayList (){ GOLD, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, JUMPGOLD },
                new ArrayList (){ null, null, null },
                new ArrayList (){ null, FLIGHT, null },
				new ArrayList (){ null, null, null },
			};


			//障害物のデザインパターンList
			//長いやつは７個目までに置く！（先端がはみ出ちゃうから）
			obstaclePattern = new ArrayList () {
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, CAR },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, null },
				new ArrayList (){ BIGFLAR, null, null },
				new ArrayList (){ null, null, BOXMEDIUM },
				new ArrayList (){ null, null, null },
                new ArrayList (){ null, null, null },
				new ArrayList (){ null, null, BOX },
			};
		}
	}
}
