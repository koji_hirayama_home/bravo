﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class LevelDesignStartPattern : MonoBehaviour
	{
        public static ArrayList startPattern = new ArrayList() {
			new LevelDesignStart1 (),
			new LevelDesignStart2 (),
			new LevelDesignStart3 (),
			new LevelDesignStart4 (),
			new LevelDesignStart5 (),
			new LevelDesignStart6 (),
            new LevelDesignStart7 (),
            new LevelDesignStart8 (),
            new LevelDesignStart9 (),
            new LevelDesignStart10 (),
            new LevelDesignStart11 (),
            new LevelDesignStart12 (),
            new LevelDesignStart13 (),
            new LevelDesignStart14 (),
			new LevelDesignStart15 (),
		};

		
	}
}
