﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Main
{


    public enum GameStatus
    {
        GAMESTART,
        GAMEPLAY,
        GAMEOVER,
        NULL
    }

    public class GameManager : MonoBehaviour
    {
        public static GameStatus gameStatus = GameStatus.NULL;

        [SerializeField] GameObject player;

        [SerializeField] GameOverManager gameOverManager;

        // [SerializeField] UIManager uIManager;
        void Awake()
        {
            Application.targetFrameRate = 30;
        }



        // Update is called once per frame
        void Update()
        {
            //ゲーム状況管理
            switch (gameStatus)
            {
                case GameStatus.GAMESTART:
                    //ゲームスタート初期設定
                    GameObject.Find("CameraController").GetComponent<CameraController>().GameStartCamera();
                    gameStatus = GameStatus.GAMEPLAY;
                    break;
                case GameStatus.GAMEPLAY:
                    //ゲームプレイ初期設定
                    //ゲームプレイ中はPlayerControllerとPlayerのRunアニメーションは必須のためtureにする
                    player.GetComponent<PlayerController>().isPlayerController = true;
                    player.GetComponent<PlayerController>().animator.SetBool("isRun", true);
                    player.GetComponent<PlayerCollision>().enabled = true;
                    StartCoroutine(AudioManager.PlayBgmCorutine());
                    //player.GetComponent<PlayerHp> ().StartHpGage ();
                    gameStatus = GameStatus.NULL;
                    break;
                case GameStatus.GAMEOVER:
                    player.GetComponent<PlayerController>().isPlayerController = false;
                    player.GetComponent<PlayerCollision>().enabled = false;
                    gameOverManager.GameOver();
                    StartCoroutine(AudioManager.GameOverBgmFadeOutCotutine());
                    gameStatus = GameStatus.NULL;
                    break;
            }
        }






        //（仮）一旦シーンを再読み込みでリスタート
        public void HomeButton()
        {
            AudioManager.Button_Sound();
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
        }

    }

}
