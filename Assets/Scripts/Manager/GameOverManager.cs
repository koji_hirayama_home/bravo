﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Main{

	public class GameOverManager : MonoBehaviour {
		[SerializeField]GameObject continueUI;//結果画面UI
		[SerializeField]GameObject resultUI;//結果画面UI
        [SerializeField]PlayerDataManager playerDataManager;
        [SerializeField] AdMobBannerInterstitial adMobBannerInterstitial;
        [SerializeField] AdMobBanner adMobBanner;
        [SerializeField] ReslutText reslutTextScript;


	// Use this for initialization
	void Start () {
			//コンティニューUIと結果画面UIの初期値をfalseにして非表示にする
			continueUI.SetActive (false);
			resultUI.SetActive (false);
	}
	
	
		//ゲームオーバー時にゲームオーバーの動作をGameManagerのゲームオーバーステータスで呼び出すよう（ゲームオーバ動作の親？？）
		public void GameOver(){
			//コルーチンでゲームオーバ時の動作を開始
			StartCoroutine (GameOverCoroutine ());
		}

		//コンティニューをアクティブ
		float ContinueImage(float timer){
			continueUI.SetActive (true);

			//コンティニューのUIパーツの動作
			continueUI.GetComponent<ContinueManager> ().FadeInContinueUI (timer);
			return timer;
		}

		//結果画面をアクティブ
		float ResultImage(float timer){
            resultUI.transform.localScale = Vector3.zero;
			resultUI.SetActive (true);
            resultUI.transform.DOScale(new Vector3(1, 1, 1), 1.2f).SetEase(Ease.OutBounce);
            playerDataManager.SaveGold();
            playerDataManager.SaveDistance();

            //バナー広告表示
            adMobBanner.ShowBanner();

			//結果画面のUIパーツの動作

			return timer;
		}

		//コンティニュー時にコルーチンを止める処理（なんかもっといい方法ある気がする、、、）
		public void StopGameOverCoroutine(){
			StopAllCoroutines ();
		}

		//ゲームオーバーの一連の流れ（動作）のコルーチン
		IEnumerator GameOverCoroutine(){
			//ゲームオーバーの初期設定（なんかやる）
			yield return new WaitForSeconds (3f);
			//コンティニュー動作のスタート処理
			//IsSkipContinue()の戻り値がtrueの時ContinueUIの表示をスキップする
			if (continueUI.GetComponent<ContinueManager>().IsSkipContinue() != true) {
				yield return new WaitForSeconds (ContinueImage (4));
				//コンティニュー動作の終了処理
				continueUI.SetActive (false);
			}

            //結果画面スタート処理
            adMobBannerInterstitial.ShowBanner(AudioManager.audio);
			yield return new WaitForSeconds (ResultImage (1.3f));
            reslutTextScript.CountUp();
           // AudioManager.audio.volume = 0.4f;
			yield break;
		}
			
	}
}
