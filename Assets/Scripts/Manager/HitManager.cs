﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Main{

	public class HitManager : MonoBehaviour {
		ScoreManager scoreManager;//ScoreManagerを使うため
		PlayerEffectController playerEffectController;//PlayerEffectControllerを使うため
		PlayerActionController playerActionController;//Playerのアクションのあるアイテム取得した時に使う


		//ヒット時のシングルトーン
		//主にヒットした時の購買作業UniRXを使う
		protected HitDispatcher hitDispatcher = HitDispatcher.Instance;

	// Use this for initialization
		void Awake(){
			scoreManager = GameObject.Find ("ScoreManager").GetComponent<ScoreManager>();
			playerEffectController = GameObject.Find ("PlayerEffect").GetComponent<PlayerEffectController> ();
			playerActionController = GameObject.Find ("Player").GetComponent<PlayerActionController> ();
		}

	void Start () {

			//障害物ヒット時のリスナー
			hitDispatcher.ObstacleHit
				.Subscribe (obstacleType => {
					Debug.Log (obstacleType);
				})
				.AddTo (this.gameObject);


			//アイテムヒット時のリスナー
			hitDispatcher.ItemHit
				.Subscribe (itemType => {
					scoreManager.GoldScore(itemType);//Goldスコア更新
					playerEffectController.PlayerEffect(itemType);//拾ったアイテムの種類によってどのエフェクトを発動させるか判定する
					playerActionController.PlayerAction(itemType);//拾ったアイテムの種類によってどのアクションを発動させるか判定する

				})
				.AddTo (gameObject);
	}
	
	

	}
}
