﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class ScoreManager : MonoBehaviour
	{
		//ゴールドゲット数
		[System.NonSerialized]public int goldScore = 0;
		//走行距離
		[System.NonSerialized]public int distanceScore = 0;


		public GameObject player;
		//走行距離取得用

		private void Start()
		{
			goldScore = 0;
			distanceScore = 0;
        }

		// Update is called once per frame
		void Update ()
		{
			//走行距離更新
			DistanceScore ();
		}

		//ゴールドスコア加算
		public void GoldScore (ItemType goldType)
		{
			//スイッチ文でゴールドのポイントを分ける
			switch (goldType) {
			case ItemType.Gold:
			case ItemType.JumpGold:
				this.goldScore += 1;
				break;
			}
		}

		//走行距離更新
		public void DistanceScore ()
		{
			distanceScore = (int)player.transform.position.z / 2;
		}
	}
}
