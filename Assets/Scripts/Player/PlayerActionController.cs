﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

	public enum PlayerActionState{
		FlightAction,
		Null,
	}

public class PlayerActionController : MonoBehaviour {
		[SerializeField]PlayerFlight playerFlight;
		[SerializeField]PlayerHp playerHp;

		public static PlayerActionState playerActionState;

	// Use this for initialization
	void Start () {
			playerActionState = PlayerActionState.Null;
	}
	
	

		public void PlayerAction(ItemType itemType){
			switch (itemType) {
			case ItemType.FlightItem:
				playerFlight.FlightGageCharge ();
				break;
			case ItemType.HpRecoveryItem:
				playerHp.HitHpRecoveryItem ();
				break;
			default:
				break;
			}
		}
	}
}
