﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class PlayerCollision : HitManager
	{
		Ray ray;
		RaycastHit hit;
		bool isHit;
		public LayerMask rayMask;
//Rayの当たり判定を制限する
		public Transform raycastPosition;
//Rayの飛ばす原点


		// Use this for initialization
		void Start ()
		{
			isHit = false;
		}
	
		
		void OnCollisionEnter (Collision col)
		{
			//正面から障害物に当たった時のみの処理
			//RayCastにヒットしているコリダーとコリジョン時に接触しているコリダーで判定
			if (col.gameObject.tag == "Obstacle" && this.GetComponent<PlayerController> ().isPlayerController == true) {
				//前方方向のみにRayを飛ばす(一応コリジョン時にもRayを飛ばす)
				ray = new Ray (raycastPosition.position, new Vector3 (0, 0, 1f));
				//Rayの半径、飛距離、Rayが影響するMask、を指定する
				if (Physics.Raycast (ray, out hit, 7, rayMask)) {
					//RayCastの処理
					isHit = true;

				}

				if (isHit == true) {
                    AudioManager.Bravo_DamageSound();
                    ShakeCamera.shake = 0.7f;
					//この辺のゲームオーバ処理を綺麗にまとめたい
					//一旦ここで障害物との衝突時にプレイヤーの動きを止める
					//Debug.Log ("hit");
					this.GetComponent<PlayerDoTweenAnime> ().LoseDoTweenAnime ();
					this.GetComponent<PlayerController> ().isPlayerController = false;
					//自身のisHitをfalseに戻して初期化する（コンティニュー時にtrueのまま始まるとコリジョンが全部Hitになってしまうため）
					this.isHit = false;
					GameManager.gameStatus = GameStatus.GAMEOVER;

				} else {
					//障害物の正面ではなくRayが当たっていなければ、横から障害物に衝突した時跳ね返る
					this.GetComponent<PlayerController> ().SideCollisionMoveToLeft (col.transform);
					this.GetComponent<PlayerController> ().SideCollisionMoveToRight (col.transform);
					this.isHit = false;
				}
			}
		}
			
	}
}
