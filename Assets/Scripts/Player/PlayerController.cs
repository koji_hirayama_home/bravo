﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

    public class PlayerController : MonoBehaviour
    {
        //Playerの動きの全制御
        [System.NonSerialized] public bool isPlayerController = false;

        const int MAX_LANE = 1;
        //右レーンへの移動範囲
        const int MIN_LANE = -1;
        //左レーンへの移動範囲
        public float laneWidth = 10.0f;
        //移動量
        int targetLane;

        [System.NonSerialized] public Animator animator;
        CharacterController controller;
        Vector3 moveDirection;


        /// <summary>
        /// インスペクター用、プレイヤーの動作をプレイヤー系スクリプトで参照用
        /// </summary>
        //重力
        public float gravity;
        //前進スピード
        public float speedZ;
        //横移動スピード
        public float speedX;
        //ジャンプ量
        public float speedJump;
        //前進加速度のパラメーター
        public float accelerationZ;

        //JUMP強制終了用に現在のグラビティーを取得しておく
        [System.NonSerialized] public float currentGravity;





        // Use this for initialization
        void Start()
        {
            controller = GetComponent<CharacterController>();
            animator = GameObject.Find("Burabor").GetComponent<Animator>();
            animator.SetBool("isRun", false);//スタート時にランアニメーションTrue

            currentGravity = gravity;//JUMP強制終了用に現在のグラビティーを取得しておく
        }

        // Update is called once per frame
        void Update()
        {
            //Trueの時にplayerの行動開始
            if (isPlayerController == true)
            {
                //デバッグ用
                if (Input.GetKeyDown("left") || TouchManager.inputDrag == InputDrag.LEFT)
                {
                    if (targetLane > MIN_LANE)
                    {
                        AudioManager.Bravo_MoveSound();
                    }

                    MoveToLeft();
                    //ドラグ操作を一度受け付けたらNULLにする（わかりやすいようにあえて４つのInputで使ってる）
                    TouchManager.inputDrag = InputDrag.NULL;
                   
                }
                if (Input.GetKeyDown("right") || TouchManager.inputDrag == InputDrag.RIGHT)
                {
                    if (targetLane < MAX_LANE)
                    {
                        AudioManager.Bravo_MoveSound();
                    }

                    MoveToRight();
                    //ドラグ操作を一度受け付けたらNULLにする（わかりやすいようにあえて４つのInputで使ってる）
                    TouchManager.inputDrag = InputDrag.NULL;
                   
                }
                if (Input.GetKeyDown("up") || TouchManager.inputDrag == InputDrag.JUMP)
                {
                    Jump();
                    //ドラグ操作を一度受け付けたらNULLにする（わかりやすいようにあえて４つのInputで使ってる）
                    TouchManager.inputDrag = InputDrag.NULL;
                }

                if (Input.GetKeyDown("down") || TouchManager.inputDrag == InputDrag.DOWN)
                {
                    Down();
                    //ドラグ操作を一度受け付けたらNULLにする（わかりやすいようにあえて４つのInputで使ってる）
                    TouchManager.inputDrag = InputDrag.NULL;
                }


                //徐々に加速しZ方向に常に前進させる
                float acceleratedZ = moveDirection.z + (accelerationZ * Time.deltaTime);
                moveDirection.z = Mathf.Clamp(acceleratedZ, 0, speedZ);

                //左右の方向は目標のポジションまでの差分の割合で速度を計算
                float ratioX = (targetLane * laneWidth - transform.position.x) / laneWidth;
                moveDirection.x = ratioX * speedX;

                //Playerの現在の状況を判定して重力の影響させるかさせないか判断する
                switch (PlayerActionController.playerActionState)
                {
                    case PlayerActionState.Null:
                        //重力分の力を毎フレーム追加
                        moveDirection.y -= gravity * Time.deltaTime;
                        break;
                    case PlayerActionState.FlightAction:
                        //フライトアクション中は重力を無効化して常にフライトアクション時の高さにする
                        moveDirection.y = 0;
                        break;
                }


                //移動実行
                Vector3 globalDirection = transform.TransformDirection(moveDirection);
                controller.Move(globalDirection * Time.deltaTime);

                //移動後設置してたらY方向の速度はリセットする
                if (controller.isGrounded)
                {
                    moveDirection.y = 0;
                    gravity = currentGravity;
                }
            }
        }

        //左のレーンに移動を開始
        void MoveToLeft()
        {
            if (targetLane > MIN_LANE)
            {
                targetLane--;
                switch (PlayerActionController.playerActionState)
                {
                    case PlayerActionState.FlightAction:
                        this.GetComponent<PlayerDoTweenAnime>().FlightMoveToLeftDoTweenAnime();
                        break;
                    default:
                        this.GetComponent<PlayerDoTweenAnime>().MoveToLeftDoTweenAnime();
                        break;
                }
            }

        }

        //右のレーンに移動を実行
        void MoveToRight()
        {
            if (targetLane < MAX_LANE)
            {
                targetLane++;
                switch (PlayerActionController.playerActionState)
                {
                    case PlayerActionState.FlightAction:
                        this.GetComponent<PlayerDoTweenAnime>().FlightMoveToRightDoTweenAnime();
                        break;
                    default:
                        this.GetComponent<PlayerDoTweenAnime>().MoveToRightDoTweenAnime();
                        break;
                }
            }

        }

        //ジャンプ
        void Jump()
        {
            if (controller.isGrounded)
            {
                moveDirection.y = speedJump;
                //ジャンプトリガーを設定
                animator.SetTrigger("Jump");

                AudioManager.Bravo_MoveSound();

            }

        }

        //下を潜る
        void Down()
        {
            if (DownTrigger.isDown == false)
            {
                animator.SetTrigger("Down");

                //JUMP中にDownアクションを実行したら普段より早く落下する
                if (controller.isGrounded == false)
                {
                    gravity = gravity * 8f;
                }
            }

        }


        //横から右にある障害物にぶつかった時の動き（跳ね返る）
        public void SideCollisionMoveToLeft(Transform CollisionTransform)
        {
            //障害物が自分の位置より右側にある時左に跳ね返る
            if (transform.position.x < CollisionTransform.position.x)
            {
                MoveToLeft();
            }
        }

        //横から左にある障害物にぶつかった時の動き（跳ね返る）
        public void SideCollisionMoveToRight(Transform CollisionTransform)
        {
            //障害物が自分の位置より右側にある時右に跳ね返る
            if (transform.position.x > CollisionTransform.position.x)
            {
                MoveToRight();
            }
        }
    }
}
