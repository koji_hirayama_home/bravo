﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main{

public class PlayerDataManager : MonoBehaviour {
        [SerializeField]ScoreManager scoreManager;
        [SerializeField] Text distanceDataText;
        [SerializeField]Text goldDataText;
        [SerializeField] ReslutText reslutTextScript;




        int goldData = 0;
        int distanceData = 0;


        [SerializeField] Material shirtMaterial;
        [SerializeField] Material pantsMaterial;
        [SerializeField] Material hierMaterial;
        int pantsData;
        int shirtData;
        int hierData;




        public Color[] shirtColor = new Color[9];
        public Color[] pantsColor = new Color[9];
        public Color[] hierColor = new Color[9];

	// Use this for initialization
	void Start () {
            //Debag用最初に所持ゴールド更新できる。
            //goldData = 10000;
            //PlayerPrefs.SetInt("GoldData", goldData);

            //PlayerData初期設定
            distanceDataText.text = PlayerPrefs.GetInt("DistanceData").ToString() + "M";
            goldDataText.text = PlayerPrefs.GetInt("GoldData").ToString();
            GetCostumeData();
	}

    // Update is called once per frame
    void Update()
    {
            
    }

        public void SaveDistance(){
			distanceData = scoreManager.distanceScore;
            if(PlayerPrefs.GetInt("DistanceData") < distanceData){
				PlayerPrefs.SetInt("DistanceData", distanceData);  
            }
        }


        public void SaveGold(){
            goldData = scoreManager.goldScore + PlayerPrefs.GetInt("GoldData");
            PlayerPrefs.SetInt("GoldData", goldData);
        }

        public void RewordGold(){
            goldData = scoreManager.goldScore + PlayerPrefs.GetInt("GoldData");
            PlayerPrefs.SetInt("GoldData", goldData);
            DOTween.To(() => reslutTextScript.goldScore, num => reslutTextScript.goldScore = num, scoreManager.goldScore * 2, 4f);
            GameObject.Find("ResultGoldScoreText").transform.DOPunchScale(new Vector3(1.04f, 1.04f, 1.04f), 3.5f);
            //scoreManager.goldScore += scoreManager.goldScore;
            AudioManager.audio.volume = 0.4f;
        }


        public void RewordGoldButton(){
            AudioManager.Button_Sound();
            GameObject.Find("AdsManager").GetComponent<UnityAdsController>().ShowUnityAds2();
        }

        public void GetGoldData(){
            goldDataText.text = PlayerPrefs.GetInt("GoldData").ToString();
        }



        //プレイヤーの現在のコスチュームを記録（ボタンを押した時に一緒に呼ぶ）
        public void SetCostumeData(ShopItemType type, int num){
            switch(type){
                case ShopItemType.SHIRT:
                    shirtMaterial.color = shirtColor[num];
                    PlayerPrefs.SetInt("CostumeData" + type.ToString(), num);
                    break;
                case ShopItemType.PANTS:
                    pantsMaterial.color = pantsColor[num];
                    PlayerPrefs.SetInt("CostumeData" + type.ToString(), num);
                    break;
                case ShopItemType.HEIR:
                    hierMaterial.color = hierColor[num];
                    PlayerPrefs.SetInt("CostumeData" + type.ToString(), num);
                    break;
            }
        }

        //コスチュームデータから現在装備中のコスチュームの呼び出し
         void GetCostumeData(){
            shirtMaterial.color = shirtColor[PlayerPrefs.GetInt("CostumeData" + ShopItemType.SHIRT.ToString(),0)];
            pantsMaterial.color = pantsColor[PlayerPrefs.GetInt("CostumeData" + ShopItemType.PANTS.ToString(),0)];
            hierMaterial.color = hierColor[PlayerPrefs.GetInt("CostumeData" + ShopItemType.HEIR.ToString(),0)];
        }
	}
}
