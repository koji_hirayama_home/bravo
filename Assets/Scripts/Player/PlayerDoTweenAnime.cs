﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Main
{
	
	public class PlayerDoTweenAnime : MonoBehaviour
	{
		[SerializeField] GameObject playerChild;
        [SerializeField] GameObject flightCameraPos;
        [SerializeField] GameObject playCameraPos;

        //DoTweenの準備
		Sequence sideMoveDoTween;
        Sequence flightAnimeDotween;



		//障害物にぶつかってゲームオーバー時のアニメーション
		public void LoseDoTweenAnime ()
		{
			//ゲームオーバー時のアニメーションどこで呼ぼう？？
			//一旦PlayerCollisionで呼ぶ

			//アニメーターのLoseアニメーションもここで行う
			playerChild.GetComponent<Animator> ().SetTrigger ("Lose");
			//ポジションが0以上だったら0にする
			if (transform.position.y > 0) {
				transform.DOLocalMoveY (0, 1f).SetEase (Ease.InCirc);
			}
		}

		//左に移動するDotweenのアニメーション
		public void MoveToLeftDoTweenAnime ()
		{
			//横移動操作のDoTweenが衝突しないようにDoTweenを削除
			sideMoveDoTween.Kill ();
			//最初に削除から始まるので毎回初期化作業
			sideMoveDoTween = DOTween.Sequence ();
			sideMoveDoTween.Append
			(
				playerChild.transform.DOLocalRotate 
				(
					new Vector3 (0, -30f, 0), 0.2f)
				.OnComplete (() =>{
					//コールバック時の作業でもDoTweenを削除してから初期化して再生（ここでもコールバック前と同じことしないとバグる）
					sideMoveDoTween.Kill ();
					sideMoveDoTween = DOTween.Sequence ();
					sideMoveDoTween.Append(playerChild.transform.DOLocalRotate (Vector3.zero, 0.4f));
				}));
		}

		//右に移動するDotweenのアニメーション
		public void MoveToRightDoTweenAnime ()
		{
			//横移動操作のDoTweenが衝突しないようにDoTweenを削除
			sideMoveDoTween.Kill ();
			//最初に削除から始まるので毎回初期化作業
			sideMoveDoTween = DOTween.Sequence ();
			sideMoveDoTween.Append 
			(
				playerChild.transform.DOLocalRotate 
				(
					new Vector3 (0, 30f, 0), 0.2f)
				.OnComplete (() => {
					//コールバック時の作業でもDoTweenを削除してから初期化して再生（ここでもコールバック前と同じことしないとバグる）
					sideMoveDoTween.Kill ();
					sideMoveDoTween = DOTween.Sequence ();
					sideMoveDoTween.Append(playerChild.transform.DOLocalRotate (Vector3.zero, 0.4f));
				}));
		}

		//空を飛ぶアイテムを取った時の空中に飛ぶアニメーション
		public void FlightDoTweenAnime(){
			this.transform.DOMoveY (GameObject.Find("PlayerFlight").GetComponent<PlayerFlight>().flightPosY, 1f);

            flightAnimeDotween.Kill();
            flightAnimeDotween = DOTween.Sequence();
            flightAnimeDotween.Append(Camera.main.transform.DOLocalMove(flightCameraPos.transform.localPosition, 1f));
            flightAnimeDotween.Join(Camera.main.transform.DOLocalRotate(flightCameraPos.transform.localEulerAngles, 1f));

		}

        public void EndFlightDoTweenAnime(){
            flightAnimeDotween.Kill();
            flightAnimeDotween = DOTween.Sequence();
            flightAnimeDotween.Append(Camera.main.transform.DOLocalMove(playCameraPos.transform.localPosition, 5f));
            flightAnimeDotween.Join(Camera.main.transform.DOLocalRotate(playCameraPos.transform.localEulerAngles, 5f));
        }


		public void FlightMoveToLeftDoTweenAnime (){
			//横移動操作のDoTweenが衝突しないようにDoTweenを削除
			sideMoveDoTween.Kill ();
			//最初に削除から始まるので毎回初期化作業
			sideMoveDoTween = DOTween.Sequence ();
			sideMoveDoTween.Append
			(
				playerChild.transform.DOLocalRotate 
				(
					new Vector3 (5, -10, 40f), 0.2f)
				.OnComplete (() =>{
					//コールバック時の作業でもDoTweenを削除してから初期化して再生（ここでもコールバック前と同じことしないとバグる）
					sideMoveDoTween.Kill ();
					sideMoveDoTween = DOTween.Sequence ();
					sideMoveDoTween.Append(playerChild.transform.DOLocalRotate (Vector3.zero, 0.3f));
				}));
		}

		public void FlightMoveToRightDoTweenAnime (){
			//横移動操作のDoTweenが衝突しないようにDoTweenを削除
			sideMoveDoTween.Kill ();
			//最初に削除から始まるので毎回初期化作業
			sideMoveDoTween = DOTween.Sequence ();
			sideMoveDoTween.Append
			(
				playerChild.transform.DOLocalRotate 
				(
					new Vector3 (5, 10, -40f), 0.2f)
				.OnComplete (() =>{
					//コールバック時の作業でもDoTweenを削除してから初期化して再生（ここでもコールバック前と同じことしないとバグる）
					sideMoveDoTween.Kill ();
					sideMoveDoTween = DOTween.Sequence ();
					sideMoveDoTween.Append(playerChild.transform.DOLocalRotate (Vector3.zero, 0.3f));
				}));
		}
	}
}
