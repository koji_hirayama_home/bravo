﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class PlayerEffectController : MonoBehaviour
	{
		//フィールドにエフェクトオブジェクトまとめる
		//Goldエフェクト
		[SerializeField] GameObject goldEffect;

		

		//エフェクト呼び出し
		public void PlayerEffect (ItemType itemType)
		{
			//接触したアイテムの種類によって別々のエフェクトを生成する
			GameObject effect = null;
			switch (itemType) {
                case ItemType.Gold:
                case ItemType.JumpGold:
				effect = Instantiate (goldEffect, Vector3.zero, Quaternion.identity, this.transform);
                    AudioManager.CoinGetSound();
				break;
			}
            if(effect != null){
                effect.transform.localPosition = Vector3.zero;
            }
		}
	}
}
