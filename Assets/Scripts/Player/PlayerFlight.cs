﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Main{

public class PlayerFlight : MonoBehaviour {
		GameObject player;
		PlayerController playerController;
		PlayerDoTweenAnime playerDoTweenAnime;
		Animator playerAnimator;
        GameObject kazeEffect;

		[SerializeField]FlightRoadInstantite flightRoadInstantite;

		//Flight時のエフェクト
		[SerializeField]ParticleSystem LeftFligthFireParticle;
		[SerializeField]ParticleSystem RightFligthFireParticle;
        [SerializeField] GameObject kazeEffectInstance;

		//FlightGageUI
		[SerializeField]GameObject flightGageUI;
		[SerializeField]Image flightGageImage;
		//フライトゲージ
		float flightGage;
		const float MaxFlightGage = 100;
		//フライトアクションをする距離
		float flightDistance;
		//フライトアイテムゲット時にどんくらいゲージを溜めるか
		[SerializeField] float flightGageChargePoint;

		//フライト時のスピード
		[SerializeField]float flightSpeed = 100f;
		//フライトスピードを加算する前のPlayer速度保存
		float playerCurrentSpeed;
		//フライトスピードの加速度を加算する前のPlayer加速度の保存
		float playerCurrentAccelerationZ;
		//フライトアクションスタート
		bool isFlightOnStart = false;
		//フライトアクション終了
		bool isFlightOnComplate = false;

		//外部スクリプト使用用
		//PlayerDoTweenAnimeScriptでフライト時の高さまで上昇させる時に使う
		public float flightPosY;



		void Awake(){
			//パーティクルを止めておく
			LeftFligthFireParticle.Stop();
			RightFligthFireParticle.Stop ();

			//フライトゲージUI初期設定
			flightGageImage.fillAmount = 0;
			flightGage = 0;
			flightGageUI.SetActive (false);
		}

	// Use this for initialization
	void Start () {
			player = GameObject.FindGameObjectWithTag ("Player");
			playerDoTweenAnime = player.GetComponent<PlayerDoTweenAnime> ();
			playerController = player.GetComponent<PlayerController> ();
			playerAnimator = GameObject.Find ("Burabor").GetComponent<Animator> ();

			//Playerの初期加速度を保存
			playerCurrentAccelerationZ = playerController.accelerationZ;

	}
	
	// Update is called once per frame
		void Update () {
			//フライトアクションスタート
			if (isFlightOnStart == true) {
				FlightOnStart ();
			}

			//フライトアクション終了準備
			if(isFlightOnComplate == true){
				//メソッド内の条件を満たしたらフライトアクション終了
				FlightOnComplete ();
			}


			//ゲーム状況がGAMEPLAYになったらフライトゲージUI可視化（ここでいいのだろうか？？雑だww一旦ここで）
			if(GameManager.gameStatus == GameStatus.GAMEPLAY){
				//flightGageUI.SetActive (true);
			}


			//フライトゲージUI周りの動作
			//フライトアクション発動前のフライトゲージUI動作
			if (PlayerActionController.playerActionState != PlayerActionState.FlightAction) {
				float gage = flightGage / MaxFlightGage;
				if (gage != flightGageImage.fillAmount) {
					flightGageImage.fillAmount = Mathf.Lerp (flightGageImage.fillAmount, gage, 0.5f);
				}
			} else {
				//フライトアクション発動時ののフライトゲージUI動作
				float flightPos = (flightRoadInstantite.maxRoadLength - player.transform.position.z) / flightDistance;
				flightGage = flightPos * 100;
				flightGageImage.fillAmount = flightGage / MaxFlightGage;
			}
		}

		//フライトアイテムゲット時にFlightGageを溜める（外部呼び出しPlayerActionController）
		public void FlightGageCharge(){
            flightGageUI.SetActive(true);
			flightGage = Mathf.Clamp (flightGage, 0, 100f);
			flightGage += flightGageChargePoint;
			if (flightGage >= MaxFlightGage) {
				flightGageImage.fillAmount = 1f;
				FlightAction ();
			}
		}


		//フライトアクション呼び出し
		public void FlightAction(){
			//FlightAction初期設定
			//Flightアクション発動時にPlayerの障害物に対してのコリジョンを無効化して無敵状態にする
			player.GetComponent<PlayerCollision> ().enabled = false;
			//フライト時のRoadDesginPart生成（引数に何回生成させるか渡す。１＝200m)
			flightRoadInstantite.InstantiteFlightRoad(7);
			//フライトアクションをする距離
			flightDistance = flightRoadInstantite.maxRoadLength - player.transform.position.z;
			//フライト時のRoadDesginPart生成後にアクションスタート
			//フライトアクションスタート！！
			isFlightOnStart = true;
            AudioManager.FlightItem_Sound();

			PlayerActionController.playerActionState = PlayerActionState.FlightAction;
		}

		void FlightOnStart(){
			if(isFlightOnStart == false){
				return;
			}

			//フライトアクションパーティクル発動
			LeftFligthFireParticle.Play();
			RightFligthFireParticle.Play ();
            KazeEffectInstance();

			//アニメーターのFlightアニメーション
			playerAnimator.GetComponent<Animator> ().SetTrigger ("Flight");
			//フライト空中に上昇アニメーション
			playerDoTweenAnime.FlightDoTweenAnime ();
			//Playerの現在のスピードを記録
			playerCurrentSpeed = playerController.speedZ;
			//Playerの現在のスピードを記録後にPlayerのスピードにフライトスピードを加える
			playerController.speedZ += flightSpeed;
			//フライト時のMaxスピード到達までの加速度
			playerController.accelerationZ += flightSpeed/2;
			//なんか前進スピードと加速度の値を加算し終わってからじゃないとうまくgravityを無効化できないからgravityの処理はStartの一番最後にやる
			playerController.gravity = 0;

			//Updateで使うため一度呼ばれたらすぐ抜ける
			isFlightOnStart = false;
			//フライトアクション終了準備をしてく。（終了メソッドの中の条件を満たしたら終了）
			isFlightOnComplate = true;
		}

		void FlightOnComplete(){
			if(isFlightOnComplate == false){
				return;
			}
			//フライトアクション中はDown操作を常に無効化
			DownTrigger.isDown = true;

			//全FlightRoadInstanceの合計の終点でフライトアクションを終わらせる。
			if (flightRoadInstantite.maxRoadLength < player.transform.position.z) {
				//gravityの値を通常時に戻す
				playerController.gravity = playerController.currentGravity;
				//Playerのスピードを通常時に戻す
				playerController.speedZ = playerCurrentSpeed;
				//Playerの加速度を通常時に戻す
				playerController.accelerationZ = playerCurrentAccelerationZ;
				//Playeのアクション状況を通常時に戻す
				PlayerActionController.playerActionState = PlayerActionState.Null;
				//フライトアクション終了と同時にPlayerの障害物に対してのコリジョンを有効化に戻す
				player.GetComponent<PlayerCollision> ().enabled = true;
				//フライトアクションパーティクルOFF
				LeftFligthFireParticle.Stop();
				RightFligthFireParticle.Stop ();
                Destroy(kazeEffect);

				//フライトゲージを０に初期化
				flightGage = 0;
				flightGageImage.fillAmount = 0;
                flightGageUI.SetActive(false);
				//アニメーターのフライト終了アニメーション（落下アニメーション）発動
				playerAnimator.SetTrigger("Folling");
				//Down操作可能に戻す
				DownTrigger.isDown = false;
                //フライトアクション終了カメラワークDoTween
                playerDoTweenAnime.EndFlightDoTweenAnime();
				//Updateで使うため一度呼ばれたらすぐ抜ける
				isFlightOnComplate = false;
			}
		}
		

        void KazeEffectInstance(){
            kazeEffect = Instantiate(kazeEffectInstance, Vector3.zero, Quaternion.identity);
            kazeEffect.transform.SetParent(player.transform);
            kazeEffect.transform.localPosition = Vector3.zero;

        }
	}
}
