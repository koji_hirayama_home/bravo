﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main
{

	public class PlayerHp : MonoBehaviour
	{
		[SerializeField]GameObject hpGageUI;
		//[SerializeField]GameObject hpRecoveryButton;
		[SerializeField]Image gageImage;
		//[SerializeField]Text recoveryItemStockText;


		const float MAXHP = 100f;
		public float hp;
		//hpの減少スピード
		public float hpDropSpeed;
		//PlayerHpの減少アニメーションの判定
		bool isPlayerHpActive = false;
		//回復アイテムの個数
		//public int stockHpRecoveryItem;

		void Awake ()
		{
			hpGageUI.SetActive (false);
			//hpRecoveryButton.SetActive (false);
		}


		// Use this for initialization
		void Start ()
		{
			hp = MAXHP;

		}
	
		// Update is called once per frame
		void Update ()
		{
			//Playerが行動可能時
			if (GetComponent<PlayerController> ().isPlayerController == true && PlayerActionController.playerActionState != PlayerActionState.FlightAction) {
				//PlayerHpの初期設定終了時に発動
				if (isPlayerHpActive) {
					hp -= Time.deltaTime * hpDropSpeed;
					hp = Mathf.Clamp (hp, 0, MAXHP);
					gageImage.fillAmount = hp / MAXHP;

				//	recoveryItemStockText.text = stockHpRecoveryItem.ToString ();

					if (hp == 0) {
						GameManager.gameStatus = GameStatus.GAMEOVER;
						//あとでやる。ここのHpがなくなった時の死亡アニメーション保留（代わりに障害物と同じ）
						this.GetComponent<PlayerDoTweenAnime> ().LoseDoTweenAnime ();
						//HPが亡くなった時にHp減少アニメーションを抜ける
						isPlayerHpActive = false;
						//回復アイテムボタンの無効化
					//	hpRecoveryButton.GetComponent<Button> ().interactable = false;
					}
				}
			}
		}

		//ゲームスタート時のHpの初期設定
		public void StartHpGage ()
		{
			//recoveryItemStockText.text = stockHpRecoveryItem.ToString ();
			//最初はUpdateでHp減少アニメーションを発動させないためfalseで初期化
			isPlayerHpActive = false;
			//Hp周りのUI可視化
			hpGageUI.SetActive (true);
			//hpRecoveryButton.SetActive (true);
			//初期設定でHpGageのfillAmountを０にする（最初にhpが上昇するアニメーションをするため）
			gageImage.fillAmount = 0;
			hp = MAXHP;
			//ゲームスタート時のHpアニメーション
			DOTween.To (
				() => gageImage.fillAmount,
				(n) => gageImage.fillAmount = n,
				1f,
				2f)
				.OnComplete (() =>{ 
					//ゲームスタート時のHpアニメーション終了後UdateでのHp減少アニメーションをスタートさせる
					isPlayerHpActive = true;
				});

			//回復アイテムを所持していれば回復ボタンを押せるようにする
			//if (stockHpRecoveryItem != 0) {
				//hpRecoveryButton.GetComponent<Button> ().interactable = true;
			//}
		}

		//回復アイテムを拾った時のアクション（Hp回復）
		public void HitHpRecoveryItem ()
		{
			if (hp != 0) {
				hp = 100;
			}
		}

		//手持ちに所持している回復アイテムのアクション
		public void StockHpRecoveryItemButton ()
		{
			//hp周りの初期設定終了後かつPlayerが行動可能時
			if (isPlayerHpActive && GetComponent<PlayerController> ().isPlayerController == true) {
				//所持している回復アイテムを減らす
				//stockHpRecoveryItem--;
				if (hp != 0) {
					hp = 100;
				}
				//所持している回復アイテムが無くなったボタンを押せなくする
//				if (stockHpRecoveryItem == 0) {
//					hpRecoveryButton.GetComponent<Button> ().interactable = false;
//				}
			}
		}
	}
}
