﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

public class PlayerSpeedLevelDesgin : MonoBehaviour {
		PlayerController playerController;
		float speedUpPos;

	// Use this for initialization
	void Start () {
			playerController = GetComponent<PlayerController> ();
			SpeedLevelUp (0);
	}
	
	// Update is called once per frame
	void Update () {
			if(transform.position.z > speedUpPos){
				SpeedLevelUp (1.5f);
				//Debug.Log (playerController.speedZ);
			}
			
		}

		void SpeedLevelUp(float pulsSpeed){
			if(playerController.speedZ < 90f){
			speedUpPos = transform.position.z + Random.Range (450, 700);
				if (PlayerActionController.playerActionState != PlayerActionState.FlightAction) {
					playerController.speedZ += pulsSpeed;
				}
			}
		}
	}
}
