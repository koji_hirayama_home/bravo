﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main{

public class DeffRoadDestroy : MonoBehaviour {
		GameObject Player;
	// Use this for initialization
	void Start () {
			Player = GameObject.Find ("Player").gameObject;
	}
	
	// Update is called once per frame
		void Update () {
			if (Player.transform.position.z > 355f) {
				Destroy (this.gameObject);
			}
		}
	}
}
