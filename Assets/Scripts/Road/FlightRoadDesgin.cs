﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class FlightRoadDesgin : MonoBehaviour
	{
		Vector3 pos = Vector3.zero;
		GameObject player;

		// Use this for initialization
		void Start ()
		{
			player = GameObject.FindGameObjectWithTag ("Player");
			RoadInstantite ();
		}
	
		

		void RoadInstantite(){
			LevelDesignCommon leveldesginCommon = (LevelDesignCommon)FlightLevelDesginPattern.flightPattern [Random.Range (0, FlightLevelDesginPattern.flightPattern.Count)];
			GetFlightRoadDesgin (leveldesginCommon.itemPattern);
		}


		void GetFlightRoadDesgin (ArrayList flightDesginPattern)
		{
			if (flightDesginPattern != null) {
				Mirror (player.GetComponent<PlayerController> ().laneWidth);
				pos.y = player.transform.Find("PlayerFlight").GetComponent<PlayerFlight> ().flightPosY;


				int desginPatternCount = flightDesginPattern.Count;
				float distance = 200f / (float)desginPatternCount;


				for (int i = 0; i < desginPatternCount; i++) {

					ArrayList desginPartList = (ArrayList)flightDesginPattern [i];

					pos.z = distance * i;

					for (int j = 0; j < desginPartList.Count; j++) {
						DesginCommon desginCommon = (DesginCommon)desginPartList [j];

						if (desginCommon != null) {
							GameObject desginPart = null;
							ItemType itemType = desginCommon.itemType;
							desginPart = Instantiate (
								(GameObject)Resources.Load (ItemInfo.itemPath [itemType]),
								Vector3.zero,
								Quaternion.identity,
								this.transform);

							desginPart.transform.localPosition = new Vector3 (j * pos.x - pos.x, pos.y, pos.z);
						}
					}
				}
			}
		}

		//ダイスを降って障害物の配置を反転させるかさせないかを決める
		void Mirror (float mirror)
		{
			int dice = Random.Range (0, 2);
			switch (dice) {
			case 0:
				pos.x = mirror;
				break;
			case 1:
				pos.x = -mirror;
				break;
			}
		}
	}
}
