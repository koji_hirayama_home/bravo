﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

	public class FlightRoadInstantite : MonoBehaviour
	{
		[SerializeField]GameObject flightRoad;
		[SerializeField]Transform player;
		//1生成にに対しての長さ
		[SerializeField]int roadLength;
		//全フライトロードインスタンスの合計の長さ（終了地点が知りたいから）
		[System.NonSerialized]public int maxRoadLength;
		int currentRoadLength;
		List<GameObject> roadList = new List<GameObject> ();

		
		// Update is called once per frame
		void Update ()
		{
			RoadDestroy ();
		}

		//Flight時のロードデザイン生成（引数に何回生成させるか渡す。１＝200m)
		public void InstantiteFlightRoad (int maxRoad)
		{
			//Playeの現在の位置に何mか足してFlightRoad生成開始いちを取得
			int currentPlayerDistance = (int)player.position.z + 200;

			for (int i = 0; i < maxRoad; i++) {
				currentRoadLength = (roadLength * i) + currentPlayerDistance;
				GameObject road = FlightRoadInstance (currentRoadLength);
				roadList.Add (road);
			}
			//フライト終了位置、フライトロード削除位置を知るために取得
			maxRoadLength = currentRoadLength + roadLength;
		}


		GameObject FlightRoadInstance (int length)
		{
			GameObject road = Instantiate (flightRoad, Vector3.zero, Quaternion.identity);
			road.transform.SetParent (this.transform);
			road.transform.localPosition = new Vector3 (0, 0, length);
			return road;
		}

		void RoadDestroy ()
		{
			if (roadList.Count != 0 && maxRoadLength + 10 < player.position.z) {
				for (int i = 0; i < roadList.Count; i++) {
					GameObject destroyRoad = roadList [i];
					roadList.RemoveAt (i);
					Destroy (destroyRoad);
				}

			}
		}
	}
}
