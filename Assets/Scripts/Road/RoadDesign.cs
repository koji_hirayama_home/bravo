﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    //デザインタイプ
    enum DesignType
    {
        ITEM,
        OBSTACLR,
    }


    public class RoadDesign : RoadDesignCommon
    {
        //オブジェクト生成位置
        Vector3 pos = Vector3.zero;

        [SerializeField]
        DesginContainer desginContainer; //重さ軽減のためのスクリプト

        protected override void Awake()
        {
            return;
        }

        // Use this for initialization
        void Start()
        {

            RoadInstantiate();
        }



        //SetとGetを使って道のデザインパターンを生成する（まとめ役）
        void RoadInstantiate()
        {
            //ダイスを降って障害物の配置を反転させるかさせないかを決める
            Mirror(GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().laneWidth);
            LevelDesignCommon levelDesignCommon = SetRoadDesign();
            GetRoadDesign(levelDesignCommon.obstaclePattern, DesignType.OBSTACLR);
            GetRoadDesign(levelDesignCommon.itemPattern, DesignType.ITEM);
        }


        //道に生成するdesignPartのレベルデザインしたスクリプトを取得する
        LevelDesignCommon SetRoadDesign()
        {
            //ここで道の生成総数の割合でレベルデザインを変える
            //(めちゃくちゃ雑だから綺麗にしたいけど一旦ここでレベルデザイン切り替える)
            LevelDesignPattern();

            //継承元にあるパターンの中を回す（パターンA,B,C,みたいな感じで難易度調整をする。パターンAが一番簡単）
            //パターンの中のグループを取得
            //グループの中身を取得（今はランダムで回す。本来はグループの中を1から順に回す）
            LevelDesignCommon levelDesignPattern = (LevelDesignCommon)levelDesignList[Random.Range(0, levelDesignList.Count)];
            return levelDesignPattern;
        }


        //SetRoadDesignで取得したレベルデザインスクリプトの中を参照してデザインパーツを生成
        void GetRoadDesign(ArrayList designPattern, DesignType designType)
        {
            if (designPattern != null)
            {
                int designPatternCount = designPattern.Count; //１次配列の数を取得
                float distance = 330f / (float)designPatternCount; //何メートル感覚で配置するかの距離

                //１次配列の中身を回す（z軸に対する動作）
                for (int i = 0; i < designPatternCount; i++)
                {
                    pos.z = distance * i; //前方方向に一定間隔の距離
                    ArrayList designPartList = (ArrayList)designPattern[i]; //２次元配列を取得

                    //2次元配列の中身を回してx軸にデザインパーツを生成する
                    for (int j = 0; j < designPartList.Count; j++)
                    {

                        DesginCommon designCommon = (DesginCommon)designPartList[j]; //2次元配列の中身を取得

                        //２次元配列の中がnullじゃなかったらオブジェクト生成作業開始
                        if (designCommon != null)
                        {
                            GameObject designPart = null;
                            //引数のデザインタイプネームでデザインタイプ（アイテムか障害物かなど）を指定。
                            //指定したデザインタイプのobjectが生成される。
                            switch (designType)
                            {
                                case DesignType.ITEM:
                                    ItemType itemType = (ItemType)designCommon.itemType;
                                    designPart = Instantiate(
                                        (GameObject)Resources.Load(ItemInfo.itemPath[itemType]),
                                        Vector3.zero,
                                        Quaternion.identity,
                                        this.transform);
                                    break;
                                case DesignType.OBSTACLR:
                                    ObstacleType obstacleType = (ObstacleType)designCommon.obstacleType;
                                    designPart = Instantiate(
                                       (GameObject)Resources.Load(ObstacleInfo.obstaclePath[obstacleType]),
                                       Vector3.zero,
                                       Quaternion.identity,
                                       this.transform);
                                    break;
                            }

                            //オブジェクト配置
                            designPart.transform.localPosition = new Vector3(j * pos.x - pos.x, pos.y, pos.z);
                            //オブジェクト格納にして非アクティブにする
                           // desginContainer.AddContainer(designPart);
                        }
                    }
                }
            }
        }

        //ダイスを降って障害物の配置を反転させるかさせないかを決める
        void Mirror(float mirror)
        {
            int dice = Random.Range(0, 2);
            switch (dice)
            {
                case 0:
                    pos.x = mirror;
                    break;
                case 1:
                    pos.x = -mirror;
                    break;
            }
        }
    }

}
