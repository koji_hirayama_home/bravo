﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
	//レベルデザインのパターンはここに追加する！！
	//スクリプト内のLevelDesignPattern()メソッドにレベルデザイン切り替え条件をかく
	public enum LevelDesign
	{
		StartDesign,
		DesignA,
		DesignB,

	}

	public class RoadDesignCommon:MonoBehaviour
	{
		//ステージのレベルデザイン変更用
        public static int levelDesignCount = 0;

		//ここに入ってくるレベルデザインパータンを入れて継承先で使う
		protected static ArrayList levelDesignList;

		//レベルデザイン切り替えパラメーター
		protected static LevelDesign levelDesign = LevelDesign.StartDesign;


		protected virtual void Awake ()
		{
			levelDesignCount = 0;//最初にカウントを０にする
			levelDesign = LevelDesign.StartDesign;
		}

	

		//デザインパターン設定
		public void LevelDesignPattern ()
		{
            //ここでレベルを切り替える判定をする
            if (levelDesignCount >= 0 && levelDesignCount < 10)
            {
                levelDesign = LevelDesign.StartDesign;
            }
            else if (levelDesignCount >= 10 && levelDesignCount < 20)
            {
                levelDesign = LevelDesign.DesignA;
                //levelDesign = LevelDesign.DesignB;
               
            }
            else if (levelDesignCount >= 20 && levelDesignCount < 100)
            {
                //levelDesign = LevelDesign.DesignA;
                levelDesign = LevelDesign.DesignB;
               
            }

			//デザインパターン設定
			switch (levelDesign) {
			case LevelDesign.StartDesign:
				levelDesignList = LevelDesignStartPattern.startPattern;
				break;
			case LevelDesign.DesignA:
				levelDesignList = LevelDesignAPattern.patternA;
				break;
			case LevelDesign.DesignB:
				levelDesignList = LevelDesignBPattern.patternB;
				break;
			}
//			Debug.Log (levelDesignCount);
			//レベルデザイン切り替えカウント更新
			levelDesignCount++;

		}
	}
}
