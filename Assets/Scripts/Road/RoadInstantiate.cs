﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{

    public enum StageType{
        Type1,
        TypeHasi,
        TypeNatu,
        TypeMati,
        TypeIwa,
        TypeCity,
    }

	public class RoadInstantiate : MonoBehaviour {

		[SerializeField]GameObject[] roadPrefabs; //生成する道（仮、あとで配列かなんかにする）
		[SerializeField]Transform player; //生成タイミングをプレイヤーの位置から判断
		[SerializeField]int roadLength; //一個あたりのロードの長さ指定
		[SerializeField]int maxRoad; //ロードの生成制限数
		List<GameObject> roadList = new List<GameObject>(); //生成したロードの管理

        int levelDesignCount = 0;

        int currentRoadIndex; //ロード生成の総数

        int stockPlayerDir; //RoadType切り替えようポジション
        int roadTypeIndex; //ロードの配列番号を取得

        public StageType stageType = StageType.Type1;


	// Use this for initialization
	void Start () {
            //初期化設定
            stockPlayerDir = 0;
            roadTypeIndex = 1;

		//スタート時にロード生成する
			InstantiateRoad(maxRoad);
			//ロード生成する前のデフォルトで置いてあったロードを消す
			//this.transform.Find ("RoadPartObjs").transform.gameObject.AddComponent<DeffRoadDestroy> ();
	}
	
	// Update is called once per frame
	void Update () {
			//キャラクターの位置を計算してロードを生成する
			int currentPlayerDistance = (int)player.position.z / roadLength;
			if (currentPlayerDistance + maxRoad > currentRoadIndex) {
				InstantiateRoad (currentPlayerDistance + maxRoad);
			}

	}

		void InstantiateRoad(int nextRoadIndex){
			//ロード生成制限数とキャラクターの位置を足してロード総数を超えていれば実行
			if (currentRoadIndex < nextRoadIndex) {
				for (int i = currentRoadIndex + 1; i <= nextRoadIndex; i++) {

					GameObject road = RoadInstance (i * roadLength);
					roadList.Add (road);

				}

				//もし一気に複数のロードを生成しても指定枚数になるようにロード削除
				while (roadList.Count > maxRoad + 2) {
					RordDestroy ();
				}

				//ロード生成総数の更新
				currentRoadIndex = nextRoadIndex;
			}

		}

		GameObject RoadInstance(int length){
            

			//ロード生成後の処理
            GameObject road = Instantiate (roadPrefabs[RoadType()], Vector3.zero, Quaternion.identity);
			road.transform.SetParent (this.transform);
            road.transform.localPosition = new Vector3(0, 0, length);

			return road;
		}

		//ロード削除
		void RordDestroy(){
			GameObject destroyRoad = roadList [0];
			roadList.RemoveAt (0);
			Destroy (destroyRoad);

		}


        int RoadType(){
            //roadPrefabsの配列の０番目はTypeHasiにする
            int currentPos = (int)player.position.z - stockPlayerDir;

            switch(stageType){
                case StageType.TypeHasi:
                    if (currentPos > Random.Range(1200,1600)){
                        stockPlayerDir = (int)player.position.z;
                        stageType = StageType.Type1;//ここに入れるのはなんでもいい。。。
                        roadTypeIndex += 1;
                        break;
            }
                    return 0;
                default:
                    if(currentPos > Random.Range(2000,2800)){
						stockPlayerDir = (int)player.position.z;
                        stageType = StageType.TypeHasi;
                        return 0;
                    }

                    break;
            }
            if (roadTypeIndex >= roadPrefabs.Length)
            {
                return roadTypeIndex = 1;
            }else{
				return roadTypeIndex;
            }
        }
	}
}
