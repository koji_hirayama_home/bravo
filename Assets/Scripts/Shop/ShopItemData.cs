﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Main{
    public enum ShopItemType{
        SHIRT,
        PANTS,
        HEIR,
    }
    
	public class ShopItemData : MonoBehaviour {
        public ShopItemType shopItemType;
        int gold;


        [SerializeField] Image itemImage;
        [SerializeField] GameObject check;
        [SerializeField] GameObject goldText;


        PlayerDataManager playerDataManager;
        ShopManager shopManager;

		
		// Use this for initialization
		void Start () {
            playerDataManager = GameObject.Find("GameManager").GetComponent<PlayerDataManager>();
            shopManager = GameObject.Find("GameManager").GetComponent<ShopManager>();
		}
		
		// Update is called once per frame
		void Update () {
           
		}

        //ショップアイテムパラメーター設定（ShopManagerで呼び出している）
        public void ShopItemImage (Sprite sprite, Color color, ShopItemType type, int gold, bool isBuy,bool isCurrentCostumeCheck,int id){
            itemImage.sprite = sprite;
            itemImage.color = color;
            shopItemType = type;
            goldText.GetComponent<Text>().text = gold.ToString();
            goldText.SetActive(!isBuy);
            check.SetActive(isCurrentCostumeCheck);
            if (PlayerPrefs.GetInt(type.ToString() + id.ToString(), 0) == id)
            {
                this.gold = 0;
            }
            else
            {
				this.gold = gold;
            }

            if (PlayerPrefs.GetInt("GoldData") < this.gold)
            {
                itemImage.gameObject.GetComponent<Button>().interactable = false;
            }
            else if(PlayerPrefs.GetInt("GoldData") >= this.gold || this.gold == 0)
            {
                itemImage.gameObject.GetComponent<Button>().interactable = true;
            }
        }



        //購入と購入データ保存
        public void Buy(int num){
            PlayerPrefs.SetInt(shopItemType.ToString() + num.ToString(), num);
            PlayerPrefs.SetInt("GoldData", PlayerPrefs.GetInt("GoldData") - gold);
            playerDataManager.SetCostumeData(shopItemType, num);
            goldText.SetActive(false);
            check.SetActive(true);

            playerDataManager.GetGoldData();
            switch(shopItemType){
                case ShopItemType.SHIRT:
                    shopManager.ShirtItemsButton();
                    break;
                case ShopItemType.PANTS:
                    shopManager.PantsItemsButton();
                    break;
                case ShopItemType.HEIR:
                    shopManager.HierItemsButton();
                    break;
            }
        }

        public void SoldOutCustumeChenge(int num){
            playerDataManager.SetCostumeData(shopItemType, num);
            check.SetActive(true);

            playerDataManager.GetGoldData();
            switch (shopItemType)
            {
                case ShopItemType.SHIRT:
                    shopManager.ShirtItemsButton();
                    break;
                case ShopItemType.PANTS:
                    shopManager.PantsItemsButton();
                    break;
                case ShopItemType.HEIR:
                    shopManager.HierItemsButton();
                    break;
            }
        }
       

	}
}

