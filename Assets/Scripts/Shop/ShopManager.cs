﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Main
{

    public class ShopManager : MonoBehaviour
    {
        public ShopItemData[] shopItemDatas = new ShopItemData[9];
        public Sprite shirtSprite;
        public Sprite pantsSprite;
        public Sprite heirSprite;

        bool isBuy;
        bool isCurrentCostumeCheck;


        int selectedId;

        [SerializeField] int shirtGold;
        [SerializeField] int pantsGold;
        [SerializeField] int hierGold;

        [SerializeField] GameObject shopUI;
        [SerializeField] PlayerDataManager playerDataManager;
        [SerializeField] GameObject buyButton;
        [SerializeField] Image buyButtonItemIage;
        [SerializeField] Text buyButtonGoldText;


        bool buttonSoundOff = true;

        // Use this for initialization
        void Start()
        {
            //初期化
			buttonSoundOff = true;//最初だけ
            buyButton.SetActive(false);
            shopUI.SetActive(false);
            ShirtItemsButton();
        }



        //アイテム表示
        public void ShirtItemsButton()
        {
            if(buttonSoundOff == false){
				AudioManager.Button_Sound();
            }
            buttonSoundOff = false;
            for (int i = 0; i < shopItemDatas.Length; i++)
            {
                ShopItemData shopItemData = shopItemDatas[i];
                ShopItemBuyCheck(ShopItemType.SHIRT, i);
                //一つ一つのパラメーター設定（値段とか色）
                shopItemData.ShopItemImage(shirtSprite, playerDataManager.shirtColor[i], ShopItemType.SHIRT, shirtGold, isBuy, isCurrentCostumeCheck, i);
            }
        }

        public void PantsItemsButton()
        {
            AudioManager.Button_Sound();
            for (int i = 0; i < shopItemDatas.Length; i++)
            {
                ShopItemData shopItemData = shopItemDatas[i];
                ShopItemBuyCheck(ShopItemType.PANTS, i);
                shopItemData.ShopItemImage(pantsSprite, playerDataManager.pantsColor[i], ShopItemType.PANTS, pantsGold, isBuy, isCurrentCostumeCheck, i);
            }
        }

        public void HierItemsButton()
        {
            AudioManager.Button_Sound();
            for (int i = 0; i < shopItemDatas.Length; i++)
            {
                ShopItemData shopItemData = shopItemDatas[i];
                ShopItemBuyCheck(ShopItemType.HEIR, i);
                shopItemData.ShopItemImage(heirSprite, playerDataManager.hierColor[i], ShopItemType.HEIR, hierGold, isBuy, isCurrentCostumeCheck, i);
            }
        }

        //チェックボタンとゴールドのBool（購入したか）
        void ShopItemBuyCheck(ShopItemType type, int num)
        {
            
            if (PlayerPrefs.GetInt(type.ToString() + num.ToString(), 0) == num)
            {
                isBuy = true;
            }
            else
            {
                isBuy = false;
            }


            if (PlayerPrefs.GetInt("CostumeData" + type.ToString(), 0) == num)
            {
                isCurrentCostumeCheck = true;
            }
            else
            {
                isCurrentCostumeCheck = false;
            }
        }

        public void BuyButton(int num)
        {
            selectedId = num;
            AudioManager.Button_Sound();
            if (PlayerPrefs.GetInt(shopItemDatas[selectedId].shopItemType.ToString() + selectedId.ToString()) != selectedId)
            {
                buyButton.SetActive(true);
                switch (shopItemDatas[selectedId].shopItemType)
                {
                    case ShopItemType.SHIRT:
                        buyButtonItemIage.sprite = shirtSprite;
                        buyButtonItemIage.color = playerDataManager.shirtColor[selectedId];
                        buyButtonGoldText.text = shirtGold.ToString();
                        break;
                    case ShopItemType.PANTS:
                        buyButtonItemIage.sprite = pantsSprite;
                        buyButtonItemIage.color = playerDataManager.pantsColor[selectedId];
                        buyButtonGoldText.text = pantsGold.ToString();
                        break;
                    case ShopItemType.HEIR:
                        buyButtonItemIage.sprite = heirSprite;
                        buyButtonItemIage.color = playerDataManager.hierColor[selectedId];
                        buyButtonGoldText.text = hierGold.ToString();
                        break;
                }
            }
            else
            {
                shopItemDatas[selectedId].SoldOutCustumeChenge(selectedId);

            }

        }

        public void BuyYesButton()
        {
            AudioManager.Button_Sound();
            shopItemDatas[selectedId].Buy(selectedId);
            buyButton.SetActive(false);
        }

        public void BuyNoButton()
        {
            AudioManager.Button_Sound();
            buyButton.SetActive(false);
            selectedId = 0;
        }



        public void ShopOpen()
        {
            AudioManager.Button_Sound();
            shopUI.SetActive(true);
            ShirtItemsButton();
        }

        public void ShopClose()
        {
            AudioManager.Button_Sound();
            buyButton.SetActive(false);
            shopUI.SetActive(false);
        }
    }
}
