﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace Main
{
	public enum InputDrag
	{
		RIGHT,
		LEFT,
		JUMP,
		DOWN,
		NULL,
	}

	public class TouchManager : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
	{
		bool isDrag = true;//ドラッグ可能か不可能かの判定


		//Input判定用
		public static InputDrag inputDrag = InputDrag.NULL;

		


		public void OnBeginDrag (PointerEventData eventData)
		{
			//ドラッグ開始、初期化
			isDrag = true;
			inputDrag = InputDrag.NULL;
		}

		public void OnDrag (PointerEventData eventData)
		{
			//ドラッグ操作を受け付けたら、一度指を放さないと動かないようにする
			if (isDrag != true) {
				return;
			}
				
			//xとyの絶対値を比較してでかい値の方のInputを優先する
			if (Mathf.Abs (eventData.delta.y) < Mathf.Abs (eventData.delta.x)) {
				if (eventData.delta.x > 1) {
					inputDrag = InputDrag.RIGHT;
					isDrag = false;
				} else if (eventData.delta.x < -1) {
					inputDrag = InputDrag.LEFT;
					isDrag = false;
				}

			} else if (Mathf.Abs (eventData.delta.x) < Mathf.Abs (eventData.delta.y)){

				if (eventData.delta.y > 1) {
					inputDrag = InputDrag.JUMP;
					isDrag = false;
				} else if (eventData.delta.y < -1) {
					inputDrag = InputDrag.DOWN;
					isDrag = false;
				}
			}
		}

		public void OnEndDrag (PointerEventData eventData)
		{
			//ドラッグ終了、初期化
			isDrag = true;
			inputDrag = InputDrag.NULL;
		}
	}
}
