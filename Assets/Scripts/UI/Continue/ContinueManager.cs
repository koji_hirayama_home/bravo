﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main
{

	public class ContinueManager : MonoBehaviour
	{
		[SerializeField]ContinueTimer continueTimer;


		//[SerializeField]Button continueButton;
		[SerializeField]Button advertisementContinueButton;
        [SerializeField] Button goldContinueButton;
        [SerializeField] Text currentGoldText;
        [SerializeField] GameObject hukkatuTimerText;
        [SerializeField] GameObject[] continueUIpart;

       

        bool isSkipContinue = false;
        public int continueGold;
		

		//コンティニュー画面のフェードイン
		public void FadeInContinueUI (float timer)
		{
			//コンティニュー画面のオブジェクトパーツの動作をまとめる
            this.gameObject.GetComponent<Image>().enabled = true;
            foreach (GameObject uiPart in continueUIpart)
            {
                uiPart.SetActive(true);
            }

			continueTimer.ContinueTimerGage (timer);
            currentGoldText.text = PlayerPrefs.GetInt("GoldData").ToString();

            if(PlayerPrefs.GetInt("GoldData") < continueGold){
                goldContinueButton.interactable = false;
                goldContinueButton.transform.gameObject.GetComponent<ScaleAnime>().enabled = false;
            }
		}




		//ゲーム内アイテムで復活（コインとか）
		public void GoldContinueButton ()
		{
            AudioManager.Button_Sound();
            StartCoroutine(ContinueCorutin());
            //バタン押せなくする。（これやらないと何度も復活できる）
            goldContinueButton.interactable = false;
            GameObject.Find("GameManager").GetComponent<GameOverManager>().StopGameOverCoroutine();
            DOTween.KillAll();
            PlayerPrefs.SetInt("GoldData", PlayerPrefs.GetInt("GoldData") - continueGold);
            goldContinueButton.transform.gameObject.GetComponent<ScaleAnime>().enabled = false;
		}


		//動画広告を見て復活
		public void ContinueAdvertisementButton ()
		{
            //まだ広告のコードなし
            //----------------------------
            AudioManager.Button_Sound();
            //ゲームオーバーコルーチンを止める
            GameObject.Find("GameManager").GetComponent<GameOverManager>().StopGameOverCoroutine();
            DOTween.KillAll();
            GameObject.Find("AdsManager").GetComponent<UnityAdsController>().ShowUnityAds();

		}

        //復活メソッド（外部呼び出し）
        public void ContinueAdvertisement(){
            StartCoroutine(ContinueCorutin());
            //バタン押せなくする。（これやらないと何度も復活できる）
            advertisementContinueButton.interactable = false;
            advertisementContinueButton.transform.gameObject.GetComponent<ScaleAnime>().enabled = false;
            AudioManager.audio.volume = 0.4f;
        }

        //復活作業コルーチン
        IEnumerator ContinueCorutin(){
            //復活タイマー可視化
            hukkatuTimerText.SetActive(true);
            Text text = hukkatuTimerText.GetComponent<Text>();

            //ContinueUIを全部非表示にする（アクティブはtrueのまま）
            this.gameObject.GetComponent<Image>().enabled = false;
            foreach (GameObject uiPart in continueUIpart)
            {
                uiPart.SetActive(false);
            }
           
            //復活時に最後に衝突した目の前の障害物が邪魔だから削除する
            DestroyContinueObjs();
           
            text.text = "3";
            yield return new WaitForSeconds(1);
            text.text = "2";
            yield return new WaitForSeconds(1);
            text.text = "1";
            yield return new WaitForSeconds(1);
            hukkatuTimerText.SetActive(false);
            //ゲームオーバーコルーチンを止める
            GameObject.Find("GameManager").GetComponent<GameOverManager>().StopGameOverCoroutine();
            //ゲームステータスにゲームプレイを渡してプレイ再開
            GameManager.gameStatus = GameStatus.GAMEPLAY;
            //PlayerのアニメーションをRunアニメーションに戻す
            GameObject.Find("Burabor").GetComponent<Animator>().SetTrigger("Continue");
            //ゲームオーバーになるまでButtonを押せなくする
            //コンティニューUIを非アクティブにする
            this.gameObject.SetActive(false);
            yield break;
        }

      

		//コンティニュー時に目の前にある障害物が邪魔だから消す
		void DestroyContinueObjs ()
		{
			//Playerと障害物の距離を図るためにPlayerを取得
			GameObject player = GameObject.Find ("Player").gameObject;
			//障害物のオブジェクトを親（ロードオブジェクト）から参照して取得するためにロードオブジェクト取得
			GameObject road = GameObject.Find ("Road").gameObject;
			//ロードオブジェクトの子から障害物のオブジェクトにたどり着くまでループ
			foreach (Transform roadPart in road.transform) {
				foreach (Transform designPart in roadPart.gameObject.transform.Find("DesignPart").transform) {
					//障害物のオブジェクトにたどり着いてからの処理
					if (designPart.gameObject.tag == "Obstacle") {
						//削除かつ障害物の位置を知るために取得
						GameObject destroyObj = designPart.gameObject;
						//Playerと障害物の距離を計算
						float destroyRadius = Vector3.Distance (player.transform.position, destroyObj.transform.position);
						//距離を計算して障害物削除
						if (destroyRadius < 100f) {
							Destroy (destroyObj);
						}
					}
				}
			}
		}

		public bool IsSkipContinue(){
			
            if(advertisementContinueButton.interactable == false && goldContinueButton.interactable == false){
				isSkipContinue = true;
			}
			return isSkipContinue;
		}
	}
}
