﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main{

	public class ContinueTimer : MonoBehaviour {
		[SerializeField]Image continueTimerGage;

	
		//コンティニュー画面のタイマーゲージ
		public void ContinueTimerGage(float timer){
			continueTimerGage.fillAmount = 1;//初期設定
			//引数で受け取った秒数でゲージを減らす
			DOTween.To (
				() => continueTimerGage.fillAmount,
				time => continueTimerGage.fillAmount = time,
				0,
				timer
			);
		}
	}
}
