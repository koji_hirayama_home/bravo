﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Main{

	public class GoldText : MonoBehaviour {
		ScoreManager scoreManager;
	// Use this for initialization
	void Start () {
			scoreManager = GameObject.Find ("ScoreManager").GetComponent<ScoreManager> ();	
	}
	
	// Update is called once per frame
		void Update () {
			GetComponent<Text> ().text = scoreManager.goldScore.ToString();
		}

	
	}
}
