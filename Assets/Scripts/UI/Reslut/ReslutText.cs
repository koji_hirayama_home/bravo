﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Main
{
    public class ReslutText : MonoBehaviour
    {
        ScoreManager scoreManager;


        [System.NonSerialized] public int distanScore;
        [System.NonSerialized] public int goldScore;

        int currentDistanScore = 0;
        int currentGoldScore = 0;


        [SerializeField] Text distanText;
        [SerializeField] Text goldText;


        // Use this for initialization
        void Start()
        {
            scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
            distanText.text = "0M";
            goldText.text = "0";
            distanScore = 0;
            goldScore = 0;
        }

        // Update is called once per frame
        void Update()
        {
            distanText.text = distanScore.ToString() + "M";
            goldText.text = goldScore.ToString();

            DistansCountUp_Sound();
            if (scoreManager.goldScore < goldScore)
            {
                GoldCountUp_Sound();
            }
        }

        public void CountUp()
        {
            DOTween.To(() => distanScore, num => distanScore = num, scoreManager.distanceScore, 4f);
            DOTween.To(() => goldScore, num => goldScore = num, scoreManager.goldScore, 4f);

        }

        void DistansCountUp_Sound()
        {
            if (distanScore > currentDistanScore + (scoreManager.distanceScore / 40))
            {
                AudioManager.CountUp_Sound();
                currentDistanScore = distanScore;
            }
            else if (distanScore == scoreManager.distanceScore && currentDistanScore < distanScore)
            {
                currentDistanScore = distanScore;
                AudioManager.CountUp_Sound();
            }
        }


        void GoldCountUp_Sound()
        {
            if (goldScore > currentGoldScore + (scoreManager.goldScore / 40))
            {
                AudioManager.CountUp_Sound();
                currentGoldScore = goldScore;
            }
            else if (goldScore == scoreManager.goldScore * 2 && currentGoldScore < goldScore)
            {
                currentGoldScore = goldScore;
                AudioManager.CountUp_Sound();
            }
        }
    }
}


