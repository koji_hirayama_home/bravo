﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Main{
	public class UIManager : MonoBehaviour {
        [SerializeField] GameObject startUI;
        [SerializeField] GameObject gamePlayUI;



		// Use this for initialization
		void Start () {
            
            gamePlayUI.SetActive(false);
            startUI.SetActive(true);
		}
		
		// Update is called once per frame
		void Update () {
			
		}

       


        public void GameStartButton()
        {
            startUI.SetActive(false);
            gamePlayUI.SetActive(true);
            GameManager.gameStatus = GameStatus.GAMESTART;
        }


       

	}
}

